package INEL;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Properties;
import java.util.regex.Pattern;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Result;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import org.apache.commons.io.FilenameUtils;
import org.jdom.input.SAXBuilder;
import org.w3c.dom.Document;
import org.jdom.output.Format;
import org.jdom.output.XMLOutputter;
import org.xml.sax.SAXException;

public class CorpusFunctions {

    public static HashMap<String, ArrayList<String>> mapExtensions(ArrayList<String> functions) {

        HashMap<String, ArrayList<String>> extensionsHash = new HashMap<>();
        for (String function : functions) {
            try {
                String classname = "INEL.validation." + function;
                Class c = Class.forName(classname);
                Object obj = c.newInstance();
                Method method = obj.getClass().getMethod("availableFor");
                ArrayList<String> inv = (ArrayList<String>) method.invoke(obj);
                extensionsHash.put(function, inv);
            } catch (ClassNotFoundException cnfe) {
                try {
                    String classname = "INEL.utilities." + function;
                    Class c = Class.forName(classname);
                    Object obj = c.newInstance();
                    Method method = obj.getClass().getMethod("availableFor");
                    ArrayList<String> inv = (ArrayList<String>) method.invoke(obj);
                    extensionsHash.put(function, inv);
                } catch (Exception e) {
                    System.out.println("DEBUG MESSAGE: " + e + "Class not found" + function);
                }
            } catch (InstantiationException ie) {
                System.out.println("Cannot instantiate the class");
            } catch (IllegalAccessException iae) {
                System.out.println("Cannot access the class");
            } catch (NoSuchMethodException nsme) {
                System.out.println("Cannot find the method");
            } catch (InvocationTargetException ite) {
                System.out.println("Cannot invoke the method");
            }
        }
        return extensionsHash;
    }

    public static void parseDir(URL inputdir, ArrayList<String> functions, HashMap<String, ArrayList<String>> extdict,
            Boolean fix, Properties properties) throws IOException, URISyntaxException {

        Pattern ignoreThose = Pattern.compile("\\.git|README|Thumbs\\.db|curation|resources|metadata|corpus\\-utilities|corpus\\-materials");

        Path dir = Paths.get(inputdir.toURI());
        Files.walk(dir).forEach(path -> {
            try {
                if (!ignoreThose.matcher(path.toFile().toString()).find()) {
                    parseFile(path.toFile(), functions, extdict, fix, properties);
                }
            } catch (IOException ex) {
                System.out.println("IOException");
            }
        });
    }

    public static void parseFile(File inputfile, ArrayList<String> functions, HashMap<String, ArrayList<String>> extdict,
            Boolean fix, Properties properties) throws IOException {

        Boolean builderNeeded = true;
        Boolean invalidXML = false;
        String filename = "";
        Document doc = null;

        if (inputfile.isFile()) {
            String extension = FilenameUtils.getExtension(inputfile.getAbsolutePath());

            for (String function : functions) {
                if (extdict.containsKey(function)) {
                    if (extdict.get(function).contains(extension)) {
                        if (builderNeeded) {
                            doc = buildXMLDocument(inputfile);
                            filename = inputfile.getName();
                            builderNeeded = false;
                            //now check if the returned value of doc is not null
                            try {
                                doc.getDocumentElement();
                            } catch (Exception e) {
                                invalidXML = true;
                                ReportItem.createReportItem(true, "XMLBuilder", filename, "Not a valid XML document");
                            }
                        }
                        if (!invalidXML) {
                            try {
                                String classname = "INEL.validation." + function;
                                Class c = Class.forName(classname);
                                Object obj = c.newInstance();
                                Method method = obj.getClass().getDeclaredMethod("runChecker", Document.class, String.class, Boolean.class, Properties.class);
                                doc = (Document) method.invoke(obj, doc, filename, fix, properties);
                                //System.out.println("SUCCESS:" + function);
                            } catch (ClassNotFoundException cnfe) {
                                try {
                                    String classname = "INEL.utilities." + function;
                                    Class c = Class.forName(classname);
                                    Object obj = c.newInstance();
                                    Method method = obj.getClass().getDeclaredMethod("runChecker", Document.class, String.class, Boolean.class, Properties.class);
                                    doc = (Document) method.invoke(obj, doc, filename, fix, properties);
                                } catch (Exception e) {
                                    System.out.println("DEBUG MESSAGE: " + e + " Class not found " + function);
                                    e.printStackTrace();
                                }
                            } catch (InstantiationException ie) {
                                System.out.println("Cannot instantiate the class");
                            } catch (IllegalAccessException iae) {
                                System.out.println("Cannot access the class");
                            } catch (NoSuchMethodException nsme) {
                                System.out.println("Cannot find the method");
                            } catch (InvocationTargetException ite) {
                                System.out.println("DEBUG MESSAGE: Cannot invoke the method " + function + " on the file " + filename);
                                ite.printStackTrace();
                            }
                        }
                    }
                }
            }

            if (fix && !builderNeeded && !invalidXML) {
                try {
                    Transformer transformer = TransformerFactory.newInstance().newTransformer();
                    Result output = new StreamResult(inputfile);
                    Source input = new DOMSource(doc);

                    transformer.transform(input, output);

                    //pretty printing
                    org.jdom.Document sf = new SAXBuilder().build(inputfile);

                    Format prettyFormat = Format.getPrettyFormat();
                    prettyFormat.setTextMode(Format.TextMode.TRIM_FULL_WHITE);
                    prettyFormat.setOmitDeclaration(false);

                    FileOutputStream fos = new FileOutputStream(inputfile);
                    XMLOutputter xmlOutputter = new XMLOutputter();
                    if (!filename.endsWith(".exs")) {
                        xmlOutputter.setFormat(prettyFormat);
                    }
                    xmlOutputter.output(sf, fos);
                    fos.close();

                } catch (Exception e) {
                    System.out.println("DEBUG MESSAGE: Caught exception " + e + ". Could not overwrite the file " + filename);
                }
            }
        }
    }

    public static Document buildXMLDocument(File inputfile) {

        Document doc = null;
        try {
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            doc = dBuilder.parse(inputfile);
            //doc.getDocumentElement().normalize();
            return doc;
        } catch (SAXException sae) {
            System.out.println("Could not build an XML document, caught SAXException");
        } catch (ParserConfigurationException pce) {
            System.out.println("Parcer Configuration Exception");
        } catch (IOException ioe) {
            System.out.println("IOException");
        }
        return doc;
    }

    public static void callXQuery(File inputdir, String queryname) {

        try {
            Class c = Class.forName("INEL.utilities.XQueryWrapper");
            Object obj = c.newInstance();
            Method method = obj.getClass().getDeclaredMethod("run", File.class, String.class);
            method.invoke(obj, inputdir, queryname);
        } catch (Exception e) {
            System.out.println("DEBUG MESSAGE:" + e);
        }
    }

    public static void zipIt(File inputdir, Properties properties) {
        try {
            Class c = Class.forName("INEL.utilities.ZipCorpus");
            Object obj = c.newInstance();
            Method method = obj.getClass().getDeclaredMethod("zip", File.class, Properties.class);
            method.invoke(obj, inputdir, properties);
        } catch (Exception e) {
            System.out.println("Error: " + e.getMessage());
            e.printStackTrace();
        }
    }

}
