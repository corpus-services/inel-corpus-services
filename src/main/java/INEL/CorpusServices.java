package INEL;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.nio.file.Paths;
import java.sql.Timestamp;

import java.util.*;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;

public class CorpusServices {

    static CommandLine cmd;
    static URL inputurl;
    static URL outputurl;
    static ArrayList<String> chosencorpusfunctions = new ArrayList<>();
    static HashMap<String, ArrayList<String>> hashExt = new HashMap<>();
    static ArrayList<ReportItem> reportOutput = new ArrayList<>();
    static Boolean fixSwitch = false;
    static Properties cfProperties = new Properties();
    static String queryname;

    public CorpusServices() {
    }

    public static void main(String[] args) throws ParseException, MalformedURLException, IOException, URISyntaxException {

        Options options = new Options();

        Option input = new Option("i", true, "input");
        input.setRequired(true);
        input.setArgName("FILE PATH");
        options.addOption(input);

        Option output = new Option("o", true, "output");
        output.setArgs(Option.UNLIMITED_VALUES);
        output.setRequired(false);
        output.setArgName("FILE PATH");
        options.addOption(output);

        Option corpusfunction = new Option("c", true, "corpusfunction");
        corpusfunction.setArgs(Option.UNLIMITED_VALUES);
        corpusfunction.setArgName("CORPUS FUNCTION");
        corpusfunction.setRequired(false);
        options.addOption(corpusfunction);

        Option utilityfunction = new Option("u", true, "utilityfunction");
        utilityfunction.setArgs(Option.UNLIMITED_VALUES);
        utilityfunction.setArgName("UTILITY FUNCTION");
        utilityfunction.setRequired(false);
        options.addOption(utilityfunction);

        Option fix = new Option("f", "fix", false, "fixes problems automatically");
        fix.setRequired(false);
        options.addOption(fix);

        Option propertyOption = Option.builder("p")
                .longOpt("property")
                .argName("property=value")
                .hasArgs()
                .valueSeparator()
                .numberOfArgs(2)
                .desc("use value for given properties")
                .build();

        options.addOption(propertyOption);

        Option xquery = new Option("x", true, "xqueryfunction");
        xquery.setRequired(false);
        options.addOption(xquery);

        CommandLineParser parser = new DefaultParser();
        cmd = parser.parse(options, args);

        String urlinputstring = cmd.getOptionValue("i");
        inputurl = Paths.get(urlinputstring).toAbsolutePath().normalize().toUri().toURL();

        if (cmd.hasOption(corpusfunction)) {
            String[] corpusfunctionarray = cmd.getOptionValues("c");
            System.out.println(Arrays.toString(corpusfunctionarray));

            if (Arrays.toString(corpusfunctionarray).equals("[INELChecks]")) {
                //first check the exbs
                chosencorpusfunctions.add("ExbEventTextContentChecker");
                chosencorpusfunctions.add("ExbFormatTableChecker");
                chosencorpusfunctions.add("ExbMcTierChecker");
                chosencorpusfunctions.add("ExbMorphemeAndGlossTiersChecker");
                chosencorpusfunctions.add("ExbRefTierChecker");
                chosencorpusfunctions.add("ExbSentenceTiersChecker");
                chosencorpusfunctions.add("ExbSpeakerTableChecker");
                chosencorpusfunctions.add("ExbStructureAndTranscriptionTierChecker");
                chosencorpusfunctions.add("ExbTiernameChecker");
                chosencorpusfunctions.add("ExbTimelineChecker");
                chosencorpusfunctions.add("ExbTranslationTiersChecker");
                chosencorpusfunctions.add("ExbUtteranceEndSymbolsChecker");
                //then do the segmentation
                chosencorpusfunctions.add("ExbSegmentationChecker");
                //then functions that deal with exs as well
                chosencorpusfunctions.add("ExbFileReferenceChecker");
                chosencorpusfunctions.add("ExbRemoveAutoSave");
                //check comafile
                chosencorpusfunctions.add("ComaApostropheChecker");
                chosencorpusfunctions.add("ComaAttachedFilepathsChecker");
                chosencorpusfunctions.add("ComaFileCoverageChecker");
                chosencorpusfunctions.add("ComaKeyTextContentChecker");
                chosencorpusfunctions.add("ComaSegmentCountChecker");
                chosencorpusfunctions.add("ComaTranscriptionsNameChecker");
                chosencorpusfunctions.add("ComaLocationsChecker");
                //and generate overviews
                chosencorpusfunctions.add("ComaOverviewGeneration");
                chosencorpusfunctions.add("ComaTierOverviewCreator");
            } else {
                chosencorpusfunctions.addAll(Arrays.asList(corpusfunctionarray));
            }
        }

        if (cmd.hasOption(utilityfunction)) {
            String[] utilityfunctionarray = cmd.getOptionValues("u");
            System.out.println(Arrays.toString(utilityfunctionarray));
            chosencorpusfunctions.addAll(Arrays.asList(utilityfunctionarray));
        }

        if (cmd.hasOption(fix)) {
            fixSwitch = true;
        }

        File inputfile = new File(inputurl.getFile());

        if (cmd.hasOption(propertyOption)) {
            cfProperties = cmd.getOptionProperties("p");
        }

        if (cmd.hasOption(xquery)) {
            queryname = cmd.getOptionValue("x");
        }

        if (chosencorpusfunctions.contains("XQueryWrapper")) {
            //may only run XQueryWrapper separately from other checks
            CorpusFunctions.callXQuery(inputfile, queryname);
            System.exit(0);
        }
        if (chosencorpusfunctions.contains("ZipCorpus")) {

            CorpusFunctions.zipIt(inputfile, cfProperties);
            System.exit(0);
        }
        if (inputfile.exists()) {
            hashExt = CorpusFunctions.mapExtensions(chosencorpusfunctions);
            System.out.println(hashExt);

            if (inputfile.isDirectory()) {
                CorpusFunctions.parseDir(inputurl, chosencorpusfunctions, hashExt, fixSwitch, cfProperties);
            } else if (inputfile.isFile()) {
                CorpusFunctions.parseFile(inputfile, chosencorpusfunctions, hashExt, fixSwitch, cfProperties);
            }
        } else {
            throw new IOException("Input URL does not exist");
        }

        if (cmd.hasOption(output)) {
            String[] urloutputstring = cmd.getOptionValues("o");
            //more than one argument -  write JSON and HTML in parallel
            if (urloutputstring.length > 1) {

                File jsonOutputFile = null;
                File htmlOutputFile = null;
                for (int i = 0; i < urloutputstring.length; i++) {
                    URL outputmultipleurl = Paths.get(urloutputstring[i]).toUri().toURL();

                    if (outputmultipleurl.toString().endsWith(".json")) {
                        jsonOutputFile = new File(outputmultipleurl.getFile());
                    } else if (outputmultipleurl.toString().endsWith(".html")) {
                        htmlOutputFile = new File(outputmultipleurl.getFile());
                    }
                }
                try (Writer jsonWriter = jsonOutputFile != null ? new OutputStreamWriter(new FileOutputStream(jsonOutputFile), StandardCharsets.UTF_8) : null;
                        Writer htmlWriter = htmlOutputFile != null ? new OutputStreamWriter(new FileOutputStream(htmlOutputFile), StandardCharsets.UTF_8) : null) {
                    Gson gson = new GsonBuilder()
                            .disableHtmlEscaping()
                            .setPrettyPrinting()
                            .create();
                    gson.toJson(reportOutput, jsonWriter);
                    String html = generateHTML(reportOutput);
                    htmlWriter.write(html);
                } catch (IOException e) {
                    System.out.println(e);
                }
                // only one output argument - check whether  it is JSON or HTML and write only one output file
            } else if (urloutputstring.length == 1) {

                for (int i = 0; i < urloutputstring.length; i++) {

                    URL outputsingleurl = Paths.get(urloutputstring[i]).toUri().toURL();
                    File outputfile = new File(outputsingleurl.getFile());

                    if (outputsingleurl.toString().endsWith(".html")) {
                        try (Writer writer = new OutputStreamWriter(new FileOutputStream(outputfile), StandardCharsets.UTF_8)) {
                            System.out.println(outputsingleurl.toString());
                            String html = generateHTML(reportOutput);
                            writer.write(html);
                        }

                    } else if (outputsingleurl.toString().endsWith(".json")) {
                        try (Writer writer = new OutputStreamWriter(new FileOutputStream(outputfile), StandardCharsets.UTF_8)) {
                            Gson gson = new GsonBuilder()
                                    .disableHtmlEscaping()
                                    .setPrettyPrinting()
                                    .create();
                            gson.toJson(reportOutput, writer);
                        }
                    }

                }
            }
        }
    }
    // if (cmd.hasOption(output)) {
    //     File outputfile = new File(outputurl.getFile());
    //    if (outputurl.toString().endsWith(".json")) {
    //        try (Writer writer = new OutputStreamWriter(new FileOutputStream(outputfile), StandardCharsets.UTF_8)) {
    //            Gson gson = new GsonBuilder()
    //                    .disableHtmlEscaping()
    //                    .setPrettyPrinting()
    //                    .create();
    //            gson.toJson(reportOutput, writer);
    //        }
    //    } else {
    //HTML report from the old corpus-services - works better than it used to
    //        try (Writer writer = new OutputStreamWriter(new FileOutputStream(outputfile), StandardCharsets.UTF_8)) {
    //            String html = generateHTML(reportOutput);
    //            writer.write(html);
    //        }
    //String report = new Gson().toJson(reportOutput);
    //    }
    //    }
    // }

    public static void appendError(ReportItem ri) {
        reportOutput.add(ri);
    }

    public static String generateHTML(Collection<ReportItem> errors) {
        System.out.println("Building html...");
        StringBuffer report = new StringBuffer();
        report.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
        report.append("<html>\n   <head>\n <title>Corpus Check Report</title>\n <meta charset=\"utf-8\"></meta>\n");

        //add JS libraries
        report.append("<script src=\"https://code.jquery.com/jquery-3.6.4.min.js\"\n integrity=\"sha256-oP6HI9z1XaZNBrJURtCoUT5SUnxFr8s3BzRl+cbzUq8=\"\n crossorigin=\"anonymous\"></script>\n");
        report.append("<script src=\"https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.21/js/jquery.dataTables.min.js\"></script>\n");
        report.append("<script src=\"https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.21/js/dataTables.bootstrap.min.js\"></script>\n");
        report.append("<script src=\"https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js\" integrity=\"sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa\" crossorigin=\"anonymous\"></script>\n");

        //add CSS
        report.append("<link rel=\"stylesheet\" href=\"https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.21/css/dataTables.bootstrap.min.css\">\n");
        report.append("<link rel=\"stylesheet\" href=\"https://cdn.datatables.net/buttons/1.1.1/css/buttons.dataTables.css\">\n");
        report.append("<link rel=\"stylesheet\"href=\"https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css\" integrity=\"sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u\" crossorigin=\"anonymous\">\n");

        //add custom CSS
        report.append("<style> body{padding:15px;} #timestamp{margin-bottom:30px;}.critical{ background:#ffdddd; }.other{ background:#ffd39e; } "
                + ".warning{ background:#fafcc2; } .char_Cyrillic{ color:#b51d0d; } .char_Greek{ background:#022299; } "
                + ".char_Armenian{ background:#ad7600; } .char_Georgian{ background:#9c026d; } "
                + "tr, td, th {border: 1px solid; } td, tr, th{padding:5px;} table{display:none; table-layout: auto; width: auto;}\n}</style>\n </head>\n   <body>\n");

        //add timestamp
        report.append("   <div id='timestamp'>Generated: ");
        Timestamp timestamp = new Timestamp(System.currentTimeMillis());
        report.append(timestamp.toString());
        report.append("</div>\n");

        report.append("<table>\n  <thead><tr>");
        report.append("<th>Type</th>");
        report.append("<th>Function</th>");
        report.append("<th>Filename:line.column</th>");
        report.append("<th>Error</th>");
        report.append("</tr></thead>\n");
        report.append("  <tbody>\n");
        for (ReportItem error : errors) {

            if (error.CriticalError) {
                report = report.append("<tr class='critical'><td style='border-left: red solid 3px'>Critical</td><td>");
            } else {
                report = report.append("<tr class='warning'><td style='border-left: yellow solid 3px'>Warning</td><td>");
            }

            report = report.append(error.CheckerName);
            report.append("</td><td>");
            report.append(error.Filename);
            report.append("</td>");
            report.append("<td style='white-space: pre'>");
            report.append(error.ErrorText);
            report.append("</td>");
            report.append("</tr>");
        }
        report.append("  </tbody>\n  </table>\n");
        report.append("<div id=\"errorlist\">");
        int critical = 0;
        int warning = 0;
        for (ReportItem error : errors) {
            if (error.CriticalError) {
                critical++;
            } else {
                warning++;
            }
        }
        HashMap<String, Integer> checkerCount = new HashMap<>();

        for (ReportItem error : errors) {
            String checkerName = error.CheckerName;
            if (checkerCount.containsKey(checkerName)) {
                checkerCount.put(checkerName, checkerCount.get(checkerName) + 1);
            } else {
                checkerCount.put(checkerName, 1);
            }
        }
        for (String checker : checkerCount.keySet()) {
            int errorCount = checkerCount.get(checker);
            report.append("<p>");
            report.append("<b>").append(checker).append("</b>").append(": ");
            report.append(errorCount).append(" errors\n");
            report.append("</p>");
            report.append("\n");

        }

        report.append("</div>");
        report.append("<div id=\"statistics\"><b>Total</b> ");
        report.append(errors.size());
        report.append(", of which: ");

        report.append(critical);
        report.append(" <b>critical</b>, ");

        report.append(warning);
        report.append(" <b>warnings</b>");
        report.append("</div>");

        //initiate DataTable on <table>
        report.append("<script>$(document).ready(function () {\n $('table').DataTable({\n 'iDisplayLength': 50,\n \"initComplete\": function(){\n $(\"table\").show();\n\n}});\n});</script>");

        //report += "   <footer style='white-space: pre'>" + summarylines + "</footer>";
        report.append("   </body>\n</html>");
        String reportString = report.toString();
        return reportString;
    }
}
