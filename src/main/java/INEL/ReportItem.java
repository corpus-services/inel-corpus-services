package INEL;

public class ReportItem {
    
    Boolean CriticalError; //critical if true, warning if false
    String CheckerName;
    String Filename;
    String ErrorText;
    
    public ReportItem(){      
    }
    
    public ReportItem(Boolean CriticalError, String CheckerName, String Filename, String ErrorText){
        this.CriticalError = CriticalError;
        this.CheckerName = CheckerName;
        this.Filename = Filename;
        this.ErrorText = ErrorText;
    }
    
    public static void createReportItem(Boolean errortype, String checker, String filename, String errortext) {
        
        ReportItem ri = new ReportItem(errortype, checker, filename, errortext);
        //String jsonString = new Gson().toJson(ri);
        //System.out.print(jsonString);
        CorpusServices.appendError(ri);
    }
    
}
