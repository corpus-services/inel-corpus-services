package INEL.utilities;

import INEL.ReportItem;
import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.UUID;
import java.util.regex.Pattern;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;
import org.w3c.dom.Element;

public class ComaMassLinkFiles {

    public ComaMassLinkFiles() {
    }
    
    public static ArrayList<String> availableFor() {
        ArrayList<String> fileTypes = new ArrayList<String> ();
        fileTypes.add("coma");
        return fileTypes;
    }
    
    public Document runChecker(Document doc, String filename, Boolean fix, Properties properties) throws SAXException, IOException, ParserConfigurationException, URISyntaxException {
        
        Boolean checkbox = false;
        Boolean linkEXB = false;
        Boolean linkEAF = false;
        Boolean linkMP3 = false;
        
        if (properties.containsKey("exb")) {
            linkEXB = true;
            checkbox = true;
        }
        
        if (properties.containsKey("eaf")) {
            linkEAF = true;
            checkbox = true;
        }
        
        if (properties.containsKey("mp3")) {
            linkMP3 = true;
            checkbox = true;
        }
        
        if (!checkbox) {
            System.out.println("You need to provide the extension of files you'd like to link as a parameter");
            return doc;
        }
        
        String baseURI = doc.getBaseURI();
        String basePath = "";
        if (baseURI.contains("/")) { //Linux path
            basePath = doc.getBaseURI().substring(0, doc.getBaseURI().lastIndexOf("/") + 1);
        } else if (baseURI.contains("\\")) { //Win path
            basePath = doc.getBaseURI().substring(0, doc.getBaseURI().lastIndexOf("\\") + 1);
        }
        
        ArrayList<String> allRefsInComa = new ArrayList<>();
        
        NodeList NSLinks = doc.getElementsByTagName("NSLink");
        for (int i = 0; i < NSLinks.getLength(); i++) {
            allRefsInComa.add(NSLinks.item(i).getTextContent());
        }
        
        NodeList relPaths = doc.getElementsByTagName("relPath");
        for (int i = 0; i < relPaths.getLength(); i++) {
            allRefsInComa.add(relPaths.item(i).getTextContent());
        }
        
        List<String> unlinkedEXBs = new ArrayList();
        List<String> unlinkedEAFs = new ArrayList();
        List<String> unlinkedMP3s = new ArrayList();
        
        URL pathURL = new URL(basePath);        
        Path corpDir = Paths.get(pathURL.toURI());
        Pattern ignoreThose = Pattern.compile("\\.git|README|Thumbs\\.db|curation|resources|metadata|corpus\\-utilities|corpus\\-materials|SegmentationErrors|\\.coma");
        Files.walk(corpDir).forEach(path -> {
            if (!ignoreThose.matcher(path.toFile().toString()).find()) {
                if (!path.toFile().isDirectory()) {
                    String fileRelPath = path.toFile().toString().replace(corpDir.toString(), "");
                    if (!allRefsInComa.contains(fileRelPath.substring(1))) {
                        if (fileRelPath.endsWith(".exb")) {
                            unlinkedEXBs.add(fileRelPath);
                        }
                        if (fileRelPath.endsWith(".eaf")) {
                            unlinkedEAFs.add(fileRelPath);
                        }
                        if (fileRelPath.endsWith(".mp3")) {
                            unlinkedMP3s.add(fileRelPath);
                        }
                    }
                }
            }
        });
             
        NodeList communications = doc.getElementsByTagName("Communication");
        for (int i = 0; i < communications.getLength(); i++) {
            Element comm = (Element) communications.item(i);
            String communicationName = comm.getAttribute("Name");
            
            if (linkEXB) {            
                for (int j = 0; j < unlinkedEXBs.size(); j++) {
                    String exb2link = unlinkedEXBs.get(j);
                    if (exb2link.substring(exb2link.lastIndexOf("/") + 1, exb2link.lastIndexOf(".")).equals(communicationName)) {
                        if (fix) {
                            Element transcription = doc.createElement("Transcription");
                            transcription.setAttribute("Id", "ID" + UUID.randomUUID().toString().toUpperCase());

                            Element trName = doc.createElement("Name");
                            trName.setTextContent(communicationName);
                            transcription.appendChild(trName);

                            Element trFilename = doc.createElement("Filename");
                            trFilename.setTextContent(communicationName + ".exb");
                            transcription.appendChild(trFilename);

                            Element trNSLink = doc.createElement("NSLink");
                            //substring skips the leading slash "/" 
                            trNSLink.setTextContent(exb2link.substring(1));
                            transcription.appendChild(trNSLink);

                            Element descr = doc.createElement("Description");
                            Element dKey = doc.createElement("Key");
                            dKey.setAttribute("Name", "segmented");
                            dKey.setTextContent("false");
                            descr.appendChild(dKey);
                            transcription.appendChild(descr);

                            Element availability = doc.createElement("Availability");
                            Element aBool = doc.createElement("Available");
                            aBool.setTextContent("false");
                            Element obtInf = doc.createElement("ObtainingInformation");
                            availability.appendChild(aBool);
                            availability.appendChild(obtInf);
                            transcription.appendChild(availability);

                            comm.appendChild(transcription);
                        } else {
                            String message = "The transcription file at " + exb2link + " may be linked to the communication named " 
                                    + communicationName;
                            ReportItem.createReportItem(false, this.getClass().getSimpleName(), filename, message);
                        }
                    }
                }
            }
            if (linkEAF) {
                for (int j=0; j < unlinkedEAFs.size(); j++) {
                    String eaf2link = unlinkedEAFs.get(j);
                    if (eaf2link.substring(eaf2link.lastIndexOf("/") + 1, eaf2link.lastIndexOf(".")).equals(communicationName)) {
                        if (fix) {
                            Element fileEl = doc.createElement("File");
                            fileEl.setAttribute("Id", "FID" + UUID.randomUUID().toString().toUpperCase());
                            
                            Element filenameEl = doc.createElement("filename");
                            filenameEl.setTextContent(communicationName + ".eaf");
                            fileEl.appendChild(filenameEl);
                            
                            Element mimetype = doc.createElement("mimetype");
                            mimetype.setTextContent("unknown");
                            fileEl.appendChild(mimetype);
                            
                            Element fileRelPath = doc.createElement("relPath");
                            //substring skips the leading slash "/" 
                            fileRelPath.setTextContent(eaf2link.substring(1));
                            fileEl.appendChild(fileRelPath);
                            
                            Element filAbsPath = doc.createElement("absPath");
                            //substring skips the leading slash "/" 
                            filAbsPath.setTextContent(eaf2link.substring(1).replaceAll("/", "\\\\"));
                            fileEl.appendChild(filAbsPath);
                            
                            Element description = doc.createElement("Description");
                            fileEl.appendChild(description);
                            
                            comm.appendChild(fileEl);
                        } else {
                            String message = "The transcription file at " + eaf2link + " may be linked to the communication named " 
                                    + communicationName;
                            ReportItem.createReportItem(false, this.getClass().getSimpleName(), filename, message);
                        }
                    }
                }
            }
            if (linkMP3) {
                for (int j=0; j < unlinkedMP3s.size(); j++) {
                    String mp32link = unlinkedMP3s.get(j);
                    if (mp32link.substring(mp32link.lastIndexOf("/") + 1, mp32link.lastIndexOf(".")).equals(communicationName)) {
                        if (fix) {
                            Element media = doc.createElement("Media");
                            media.setAttribute("ID", "MID" + UUID.randomUUID().toString().toUpperCase());
                            
                            Element descr = doc.createElement("Description");
                            Element key = doc.createElement("Key");
                            key.setAttribute("Name", "Type");
                            key.setTextContent("Digital");
                            descr.appendChild(key);
                            media.appendChild(descr);
                            
                            Element fnameEl = doc.createElement("Filename");
                            fnameEl.setTextContent(communicationName + ".mp3");
                            media.appendChild(fnameEl);
                            
                            Element nslink = doc.createElement("NSLink");
                            nslink.setTextContent(mp32link.substring(1));
                            media.appendChild(nslink);
                            
                            NodeList recordings = comm.getElementsByTagName("Recording");
                            for (int k = 0; k < recordings.getLength(); k++) {
                                Element recording = (Element) recordings.item(k);
                                if (recording.getElementsByTagName("Name").item(0).getTextContent().equals(communicationName)) {
                                    recording.appendChild(media);
                                    break;
                                }
                            }
                        } else {
                            String message = "The audio file at " + mp32link + " may be linked to the communication named " 
                                    + communicationName;
                            ReportItem.createReportItem(false, this.getClass().getSimpleName(), filename, message);
                        }
                    }
                }
            }
        }
        
        return doc;
    }      
}
