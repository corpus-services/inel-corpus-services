package INEL.utilities;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Properties;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathFactory;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

public class CorpusHTML {

    public CorpusHTML() {
    }
    
    public static ArrayList<String> availableFor() {
        ArrayList<String> fileTypes = new ArrayList<String> ();
        fileTypes.add("coma");
        return fileTypes;
    }
    
    public Document runChecker(Document doc, String filename, Boolean fix, Properties properties) throws SAXException, IOException, ParserConfigurationException, URISyntaxException {
        
        String xslpath = "/xsl/coma2html.xsl";    
        String xsl;
        
        try {
            URL docURL = new URL(doc.getBaseURI());
            File flextext = new File(docURL.getFile());
            String xml = FileUtils.readFileToString(flextext, "utf-8");

            // get the XSLT stylesheet as String            
            java.util.Scanner s = new java.util.Scanner(getClass().getResourceAsStream(xslpath)).useDelimiter("\\A");
            xsl = s.hasNext() ? s.next() : "";
            
            // create XSLTransformer and set the parameters 
            XSLTransformer xt = new XSLTransformer();
            
            // get the corpus name and its verstion from coma
            XPath xp = XPathFactory.newInstance().newXPath();
            String XPathName = "//Key[@Name='inel:corpusPrefix']";
            NodeList matchName = (NodeList) xp.compile(XPathName).evaluate(doc, XPathConstants.NODESET);
            String XPathVersion = "//Key[@Name='inel:corpusVersion']";
            NodeList matchVersion = (NodeList) xp.compile(XPathVersion).evaluate(doc, XPathConstants.NODESET);
            
            if (matchName.getLength() == 1 && matchVersion.getLength() == 1) {
                String corpusPrefix = matchName.item(0).getTextContent();
                String corpusVersion = matchVersion.item(0).getTextContent();

                xt.setParameter("identifier", "spoken-corpus:" + corpusPrefix + "-" + corpusVersion);
                // perform XSLT transformation

                String result = xt.transform(xml, xsl);
                //get location to save new result
                String outfile = FilenameUtils.getFullPath(doc.getBaseURI()) + "corpus.html";
                URL overviewurl = new URL(outfile);
                File outputfile = new File(overviewurl.getFile());

                try (Writer writer = new OutputStreamWriter(new FileOutputStream(outputfile), StandardCharsets.UTF_8)) {
                    writer.write(result);
                }
            } else {
                System.out.println("ERROR: keys inel:corpusVersion and/or inel:corpusName are missing from the comafile. Change it and try again...");
            }

        } catch (TransformerConfigurationException ex) {
            System.out.println("Transformer configuration error");
        } catch (TransformerException ex) {
            System.out.println("Transformer error");
        } catch (MalformedURLException ex) {
            System.out.println("Malformed URL error");
        } catch (IOException ex) {
            System.out.println("Unknown input/output error");
        } catch (Exception e) {
            System.out.println("Caught " + e);
        } 
              
        return doc;
    }      
}
