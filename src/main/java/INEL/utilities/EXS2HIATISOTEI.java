package INEL.utilities;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.xpath.XPathExpressionException;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.exmaralda.common.corpusbuild.TextFilter;
import org.exmaralda.common.jdomutilities.IOUtilities;
import org.jdom.Attribute;
import org.jdom.JDOMException;
import org.jdom.input.SAXBuilder;
import org.jdom.Document;
import org.jdom.Element;
import org.jdom.Namespace;
import org.jdom.Text;
import org.jdom.output.Format;
import org.jdom.output.XMLOutputter;
import org.jdom.xpath.XPath;
import org.xml.sax.SAXException;


public class EXS2HIATISOTEI {
    
    static String TEI_SKELETON_STYLESHEET_ISO = "/xsl/EXMARaLDA2ISOTEI_Skeleton.xsl";
    static String SC_TO_TEI_U_STYLESHEET_ISO = "/xsl/SegmentChain2ISOTEIUtterance.xsl";
    static String SORT_AND_CLEAN_STYLESHEET_ISO = "/xsl/ISOTEICleanAndSort.xsl";
    static String TIME2TOKEN_SPAN_REFERENCES = "/xsl/time2tokenSpanReferences.xsl";
    static String REMOVE_TIME = "/xsl/removeTimepointsWithoutAbsolute.xsl";
    static String SPANS2_ATTRIBUTES = "/xsl/spans2attributes.xsl";
    
    String nameOfDeepSegmentation = "SpeakerContribution_Utterance_Word";
    String nameOfFlatSegmentation = "SpeakerContribution_Event";
    
    static String BODY_NODE = "//text";
    String language = "en";
    
    public EXS2HIATISOTEI() {
    }
    
    public static ArrayList<String> availableFor() {
        ArrayList<String> fileTypes = new ArrayList<String> ();
        fileTypes.add("exs");
        return fileTypes;
    }
    
    public org.w3c.dom.Document runChecker(org.w3c.dom.Document doc, String filename, Boolean fix, Properties properties) throws SAXException, IOException, 
                ParserConfigurationException, TransformerConfigurationException, TransformerException, XPathExpressionException, JDOMException,
                URISyntaxException {
        
        SAXBuilder builder = new SAXBuilder();
        URL docURL = new URL(doc.getBaseURI());
        Document exsdoc = builder.build(docURL);
        
        Document finalDocument = null;
        
        String skeleton_stylesheet = getXSLResourceAsString(TEI_SKELETON_STYLESHEET_ISO);
        String transform_stylesheet = getXSLResourceAsString(SC_TO_TEI_U_STYLESHEET_ISO);
        String sort_and_clean_stylesheet = getXSLResourceAsString(SORT_AND_CLEAN_STYLESHEET_ISO);
        String time_2_token_stylesheet = getXSLResourceAsString(TIME2TOKEN_SPAN_REFERENCES);
        String remove_time_stylesheet = getXSLResourceAsString(REMOVE_TIME);
        String spans_2_attributes_stylesheet = getXSLResourceAsString(SPANS2_ATTRIBUTES);
        
        org.jdom.Document teiDocument = null;
        
        XSLTransformer xslt = new XSLTransformer();
        
        File exs = new File(docURL.getFile());
        String xml = FileUtils.readFileToString(exs, "utf-8");
        String result = xslt.transform(xml, skeleton_stylesheet);
        
        if (result != null) {
            //now we get a document of the first transformation, the iso tei skeleton
            teiDocument = String2JdomDocument(result);
            System.out.println("STEP 1 completed.");
            
            ArrayList uElements = TEIMerge(exsdoc, nameOfDeepSegmentation, nameOfFlatSegmentation);
            
            BODY_NODE = "//tei:body";
            XPath xp = XPath.newInstance(BODY_NODE);
            xp.addNamespace("tei", "http://www.tei-c.org/ns/1.0");

            Element textNode = (Element) (xp.selectSingleNode(teiDocument));
            textNode.addContent(uElements);
            if (teiDocument != null) {
                System.out.println("STEP 2 completed.");
                Document transformedDocument = null;
                xslt.setParameter("mode", "inel");
                String result2 = xslt.transform(JdomDocument2String(teiDocument), transform_stylesheet);
                transformedDocument = IOUtilities.readDocumentFromString(result2);
                if (transformedDocument != null) {
                    textNode = (Element) (xp.selectSingleNode(transformedDocument));
                    System.out.println("STEP 3 completed.");
                    // now take care of the events from tiers of type 'd'
                    XPath xp2 = XPath.newInstance("//segmentation[@name='Event']/ats");
                    List events = xp2.selectNodes(exsdoc);
                    for (int pos = 0; pos < events.size(); pos++) {
                        Element exmaraldaEvent = (Element) (events.get(pos));
                        String category = exmaraldaEvent.getParentElement().getParentElement().getAttributeValue("category");

                        String elementName = "event";
                        if (category.equals("pause")) {
                            elementName = "pause";
                        }

                        Element teiEvent = new Element(elementName);

                        String speakerID = exmaraldaEvent.getParentElement().getParentElement().getAttributeValue("speaker");
                        if (speakerID != null) {
                            teiEvent.setAttribute("who", speakerID);
                        }
                        teiEvent.setAttribute("start", exmaraldaEvent.getAttributeValue("s"));
                        teiEvent.setAttribute("end", exmaraldaEvent.getAttributeValue("e"));
                        if (!category.equals("pause")) {
                            teiEvent.setAttribute("desc", exmaraldaEvent.getText());
                            teiEvent.setAttribute("type", category);
                        } else {
                            String duration = exmaraldaEvent.getText().replaceAll("\\(", "").replaceAll("\\)", "");
                            teiEvent.setAttribute("dur", duration);
                        }
                        textNode.addContent(teiEvent);
                    }
                    
                    String result4
                            = xslt.transform(JdomDocument2String(transformedDocument), time_2_token_stylesheet);
                    String result5
                            = xslt.transform(result4, remove_time_stylesheet);
                    String result6
                            = xslt.transform(result5, spans_2_attributes_stylesheet);
                    transformedDocument = IOUtilities.readDocumentFromString(result6);

                    //generate element ids
                    generateWordIDs(transformedDocument);
                    if (transformedDocument != null) {
                        //Here the annotations are taken care of
                        //this is important for the INEL morpheme segmentations
                        //for the INEL transformation, the word IDs are generated earlier
                        String result3
                                = xslt.transform(JdomDocument2String(transformedDocument), sort_and_clean_stylesheet);
                        if (result3 != null) {
                            finalDocument = IOUtilities.readDocumentFromString(result3);
                            if (finalDocument != null) {
                                System.out.println("Merged");
                                //so is the language of the doc
                                setDocLanguage(finalDocument, language);
                                //now the completed document is saved
                                //save next to the old file
                                String outfile = FilenameUtils.getFullPath(doc.getBaseURI()) + filename.substring(0, filename.lastIndexOf("_")) + "_tei.xml";
                                URL outurl = new URL(outfile);
                                //URL outurl = new URL("file://" + filename.substring(0, filename.lastIndexOf(".")) + "_tei.xml");
                                System.out.println(outurl.toString());
                                final File ouf = new File(outurl.toURI());
                                if (!ouf.exists()) {
                                    ouf.createNewFile();
                                }
                                try {
                                    Format prettyFormat = Format.getPrettyFormat();
                                    prettyFormat.setTextMode(Format.TextMode.TRIM_FULL_WHITE);
                                    prettyFormat.setOmitDeclaration(false);
                                    
                                    FileOutputStream fos = new FileOutputStream(ouf);
                                    XMLOutputter xmlOutputter = new XMLOutputter(prettyFormat);
                                    xmlOutputter.output(finalDocument, fos);
                                    fos.close();
                                    System.out.println("document written.");
                                } catch (Exception ex) {
                                    System.out.print("Caught " + ex + " when writing the segmented transcription file");
                                }
                            }
                        }
                    }
                }
            }
        }        
        
        return doc;
    }
    
    static ArrayList TEIMerge(Document segmentedTranscription, String nameOfDeepSegmentation, String nameOfFlatSegmentation) {
        try {
            // Make a map of the timeline
            HashMap timelineItems = new HashMap();
            
            String xpath = "//tli";
            XPath xpx = XPath.newInstance(xpath);
            List tlis = xpx.selectNodes(segmentedTranscription);
            for (int pos = 0; pos < tlis.size(); pos++) {

                timelineItems.put(((Element) (tlis.get(pos))).getAttributeValue("id"), pos);
            }
            
            ArrayList returnValue = new ArrayList();
            XPath xp1 = XPath.newInstance("//segmentation[@name='" + nameOfDeepSegmentation + "']/ts");
            List segmentChains = xp1.selectNodes(segmentedTranscription);
            
            // go through all top level segment chains
            for (Object segmentChain : segmentChains) {
                Element sc = (Element) (segmentChain);
                sc.setAttribute("speaker", sc.getParentElement().getParentElement().getAttributeValue("speaker"));
                String tierref = sc.getParentElement().getAttributeValue("tierref");
                String start = sc.getAttributeValue("s");
                String end = sc.getAttributeValue("e");
                String xpath2 = "//segmentation[@name='" + nameOfFlatSegmentation + "' and @tierref='" + tierref + "']"
                        + "/ts[@s='" + start + "' and @e='" + end + "']";
                XPath xp2 = XPath.newInstance(xpath2);
                
                Element sc2 = (Element) (xp2.selectSingleNode(segmentedTranscription));
                if (sc2 == null) {
                    //this means that no corresponding top level
                    //element was found in the second segmentation
                    //which should not happen
                    throw new Exception(tierref + " " + start + " " + end);
                }
                // this is where the magic happens
                Element mergedElement = merge(sc, sc2);
                
                // now take care of the corresponding annotations
                int s = ((Integer) (timelineItems.get(start)));
                int e = ((Integer) (timelineItems.get(end)));
                
                String xpath3 = "//segmentation[@name='" + nameOfFlatSegmentation + "' and @tierref='" + tierref + "']"
                            + "/ts[@s='" + start + "' and @e='" + end + "']/ts";
                    XPath xp3 = XPath.newInstance(xpath3);
                    List transannos = xp3.selectNodes(segmentedTranscription);
                    for (Object transanno1 : transannos) {
                        Element transanno = (Element) transanno1;
                        String transaStart = transanno.getAttributeValue("s");
                        String transaEnd = transanno.getAttributeValue("e");
                        int transas = ((Integer) (timelineItems.get(transaStart)));
                        int transae = ((Integer) (timelineItems.get(transaEnd)));
                        boolean transannotationBelongsToThisElement = (transas >= s && transas <= e) || (transae >= s && transae <= e);
                        if (transannotationBelongsToThisElement) {
                            Element annotationsElement = mergedElement.getChild("annotations");
                            if (annotationsElement == null) {
                                annotationsElement = new Element("annotations");
                                mergedElement.addContent(annotationsElement);
                            }
                            Element annotation = new Element("annotation");
                            annotation.setAttribute("start", transaStart);
                            annotation.setAttribute("end", transaEnd);
                            annotation.setAttribute("level", transanno.getParentElement().getParentElement().getAttributeValue("name"));
                            annotation.setAttribute("value", transanno.getText());
                            annotationsElement.addContent(annotation);
                        }
                    }
                    
                // now take care of the corresponding annotations
                String xpath5 = "//segmented-tier[@id='" + tierref + "']/annotation/ta";
                XPath xp5 = XPath.newInstance(xpath5);
                List annotations = xp5.selectNodes(segmentedTranscription);
                for (Object annotation1 : annotations) {
                    Element anno = (Element) (annotation1);
                    String aStart = anno.getAttributeValue("s");
                    String aEnd = anno.getAttributeValue("e");
                    int as = ((Integer) (timelineItems.get(aStart)));
                    int ae = ((Integer) (timelineItems.get(aEnd)));
                    boolean annotationBelongsToThisElement = (as >= s && as <= e) || (ae >= s && ae <= e);
                    if (annotationBelongsToThisElement) {
                        Element annotationsElement = mergedElement.getChild("annotations");
                        if (annotationsElement == null) {
                            annotationsElement = new Element("annotations");
                            mergedElement.addContent(annotationsElement);
                        }
                        Element annotation = new Element("annotation");
                        annotation.setAttribute("start", aStart);
                        annotation.setAttribute("end", aEnd);
                        annotation.setAttribute("level", anno.getParentElement().getAttributeValue("name"));
                        annotation.setAttribute("value", anno.getText());
                        annotationsElement.addContent(annotation);
                    }
                }
                
                //include full text
                Element annotation = new Element("annotation");
                annotation.setAttribute("start", start);
                annotation.setAttribute("end", end);
                annotation.setAttribute("level", "full-text");

                String fullText = "";
                List l = XPath.selectNodes(sc2, "descendant::text()");
                for (Object o : l) {
                    Text text = (Text) o;
                    fullText += text.getText();
                }
                annotation.setAttribute("value", fullText);

                Element annotationsElement = mergedElement.getChild("annotations");
                if (annotationsElement == null) {
                    annotationsElement = new Element("annotations");
                    mergedElement.addContent(annotationsElement);
                }
                annotationsElement.addContent(annotation);
                returnValue.add(mergedElement.detach());
            }
            
            return returnValue;
            
        } catch (JDOMException ex) {
            ex.printStackTrace();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return null;
    }
    
    static Element merge(Element e1, Element e2) {
        
        Iterator i1 = e1.getDescendants();
        ArrayList pcData1 = new ArrayList();
        while (i1.hasNext()) {
            pcData1.add(i1.next());
        }

        Iterator i2 = e2.getDescendants(new TextFilter());
        ArrayList pcData2 = new ArrayList();
        while (i2.hasNext()) {
            pcData2.add(i2.next());
        }
        
        int charBoundary = 0;
        for (int pos = 0; pos < pcData2.size() - 1; pos++) {
            Text eventText = (Text) (pcData2.get(pos));
            Element anchor = new Element("anchor");
            Element event = eventText.getParentElement();
            String start = event.getAttributeValue("e");
            anchor.setAttribute("synch", start);

            charBoundary += eventText.getText().length();
            // jetzt durch den anderen Baum laufen und den zugehoerigen Anker
            // an der richtigen Stelle einfuegen
            int charCount = 0;
            for (int pos2 = 0; pos2 < pcData1.size(); pos2++) {
                Object o = pcData1.get(pos2);
                if (!(o instanceof Text)) {
                    continue;
                }
                Text segmentText = (Text) o;
                int textLength = segmentText.getText().length();
                if (charCount + textLength < charBoundary) {
                    charCount += textLength;
                    continue;
                } else if (charCount + textLength == charBoundary) {
                    Element parent = segmentText.getParentElement();
                    int index = parent.indexOf(segmentText);
                    Element parentOfParent = parent.getParentElement();
                    int index2 = parentOfParent.indexOf(parent);
                    parentOfParent.addContent(index2 + 1, anchor);
                    break;
                }
                // charCount+textLength>charBoundary
                String leftPart = segmentText.getText().substring(0, charBoundary - charCount);
                String rightPart = segmentText.getText().substring(charBoundary - charCount);
                Text leftText = new Text(leftPart);
                Text rightText = new Text(rightPart);

                // neue Sachen muessen zweimal eingefuegt werden - einmal
                // in den Vector, einmal in den Parent
                // Sachen im Vector muessen den richtigen Parent bekommen
                Element parent = segmentText.getParentElement();
                parent.removeContent(segmentText);
                parent.addContent(leftText);
                parent.addContent(anchor);
                parent.addContent(rightText);

                pcData1.remove(segmentText);
                pcData1.add(pos2, rightText);
                pcData1.add(pos2, anchor);
                pcData1.add(pos2, leftText);
                break;
            }
        }

        return e1;
    }
    
    private void generateWordIDs(Document document) throws JDOMException {
        // added 30-03-2016
        HashSet<String> allExistingIDs = new HashSet<String>();
        XPath idXPath = XPath.newInstance("//tei:*[@xml:id]");
        idXPath.addNamespace("tei", "http://www.tei-c.org/ns/1.0");
        idXPath.addNamespace(Namespace.XML_NAMESPACE);
        List idElements = idXPath.selectNodes(document);
        for (Object o : idElements) {
            Element e = (Element) o;
            allExistingIDs.add(e.getAttributeValue("id", Namespace.XML_NAMESPACE));
        }

        // changed 30-03-2016
        XPath wordXPath = XPath.newInstance("//tei:w[not(@xml:id)]");
        wordXPath.addNamespace("tei", "http://www.tei-c.org/ns/1.0");
        wordXPath.addNamespace(Namespace.XML_NAMESPACE);

        List words = wordXPath.selectNodes(document);
        int count = 1;
        for (Object o : words) {
            Element word = (Element) o;
            while (allExistingIDs.contains("w" + Integer.toString(count))) {
                count++;
            }

            String wordID = "w" + Integer.toString(count);
            allExistingIDs.add(wordID);
            //System.out.println("*** " + wordID);
            word.setAttribute("id", wordID, Namespace.XML_NAMESPACE);
        }

        // new 02-12-2014
        XPath pcXPath = XPath.newInstance("//tei:pc[not(@xml:id)]");
        pcXPath.addNamespace("tei", "http://www.tei-c.org/ns/1.0");
        pcXPath.addNamespace(Namespace.XML_NAMESPACE);

        List pcs = pcXPath.selectNodes(document);
        count = 1;
        for (Object o : pcs) {
            Element pc = (Element) o;
            while (allExistingIDs.contains("pc" + Integer.toString(count))) {
                count++;
            }

            String pcID = "pc" + Integer.toString(count);
            allExistingIDs.add(pcID);
            //System.out.println("*** " + wordID);
            pc.setAttribute("id", pcID, Namespace.XML_NAMESPACE);
        }

        // we also need this for events/incidents
        XPath incXPath = XPath.newInstance("//tei:event[not(@xml:id)]");
        pcXPath.addNamespace("tei", "http://www.tei-c.org/ns/1.0");
        pcXPath.addNamespace(Namespace.XML_NAMESPACE);

        List incs = incXPath.selectNodes(document);
        count = 1;
        for (Object o : incs) {
            Element pc = (Element) o;
            while (allExistingIDs.contains("inc" + Integer.toString(count))) {
                count++;
            }

            String incID = "inc" + Integer.toString(count);
            allExistingIDs.add(incID);
            //System.out.println("*** " + wordID);
            pc.setAttribute("id", incID, Namespace.XML_NAMESPACE);
        }

        // we also need this for seg elements
        XPath segXPath = XPath.newInstance("//tei:seg[not(@xml:id)]");
        pcXPath.addNamespace("tei", "http://www.tei-c.org/ns/1.0");
        pcXPath.addNamespace(Namespace.XML_NAMESPACE);

        List segs = segXPath.selectNodes(document);
        count = 1;
        for (Object o : segs) {
            Element seg = (Element) o;
            while (allExistingIDs.contains("seg" + Integer.toString(count))) {
                count++;
            }

            String segID = "seg" + Integer.toString(count);
            allExistingIDs.add(segID);
            //System.out.println("*** " + wordID);
            seg.setAttribute("id", segID, Namespace.XML_NAMESPACE);
        }
    }
    
    private void setDocLanguage(Document teiDoc, String language) throws JDOMException {
        // /TEI/text[1]/@*[namespace-uri()='http://www.w3.org/XML/1998/namespace' and local-name()='lang']
        XPath xpathToLangAttribute = XPath.newInstance("//tei:text/@xml:lang");
        xpathToLangAttribute.addNamespace("tei", "http://www.tei-c.org/ns/1.0");
        xpathToLangAttribute.addNamespace(Namespace.XML_NAMESPACE);
        Attribute langAtt = (Attribute) xpathToLangAttribute.selectSingleNode(teiDoc);
        if (langAtt != null) {
            langAtt.setValue(language);
        } else {
            XPath xpathToTextElement = XPath.newInstance("//tei:text");
            xpathToTextElement.addNamespace("tei", "http://www.tei-c.org/ns/1.0");
            xpathToTextElement.addNamespace(Namespace.XML_NAMESPACE);
            Element textEl = (Element) xpathToTextElement.selectSingleNode(teiDoc);
            textEl.setAttribute("lang", language, Namespace.XML_NAMESPACE);
        }
        System.out.println("Language of document set to " + language);

    }
    
    private String getXSLResourceAsString(String path2xsl) {
        
        String xslString;
        java.util.Scanner scan = new java.util.Scanner(getClass().getResourceAsStream(path2xsl)).useDelimiter("\\A");
        xslString = scan.hasNext() ? scan.next() : "";
        return xslString;
    }
    
    private Document String2JdomDocument(String stringRespresentingDocument) {
        Document newDocument = null;
        try {
            InputStream stream = null;
            SAXBuilder builder = new SAXBuilder();
            stream = new ByteArrayInputStream(stringRespresentingDocument.getBytes("UTF-8"));
            newDocument = builder.build(stream);
        } catch (UnsupportedEncodingException ex) {
            Logger.getLogger(EXS2HIATISOTEI.class.getName()).log(Level.SEVERE, null, ex);
        } catch (JDOMException ex) {
            Logger.getLogger(EXS2HIATISOTEI.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(EXS2HIATISOTEI.class.getName()).log(Level.SEVERE, null, ex);
        }
        return newDocument;
    }
    
    private String JdomDocument2String(org.jdom.Document jdomDocument) {
        return new XMLOutputter().outputString(jdomDocument);
    }
}
