package INEL.utilities;

import INEL.ReportItem;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Properties;
import java.util.UUID;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

public class EafCleanEmptyMedia {

    public EafCleanEmptyMedia() {
    }
    
    public static ArrayList<String> availableFor() {
        ArrayList<String> fileTypes = new ArrayList<String> ();
        fileTypes.add("eaf");
        return fileTypes;
    }
    
    public Document runChecker(Document doc, String filename, Boolean fix, Properties properties) throws SAXException, IOException, ParserConfigurationException {
        
        NodeList header = doc.getElementsByTagName("HEADER");
        
        for (int i = 0; i < header.getLength(); i++) {
            Element head = (Element) header.item(i);
            
            if (head.getElementsByTagName("MEDIA_DESCRIPTOR").getLength() > 0) {
                Node mediaDesc = head.getElementsByTagName("MEDIA_DESCRIPTOR").item(0);
                Element mediaEl = (Element) mediaDesc;

                if (mediaEl.getAttribute("MEDIA_URL").endsWith("file:///")) {
                    if (fix) {
                        head.removeChild(mediaEl);
                        
                        Element property = doc.createElement("PROPERTY");
                        property.setAttribute("NAME", "URN");
                        property.setTextContent("urn:nl-mpi-tools-elan-eaf:" + UUID.randomUUID().toString());
                        head.appendChild(property);
                    } else {
                        String message = "May clean media_descriptor for a non-existing audio.";
                        ReportItem.createReportItem(false, this.getClass().getSimpleName(), filename, message);
                    }
                }
            }
        }
        
        return doc;
    }      
}
