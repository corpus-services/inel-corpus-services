package INEL.utilities;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Properties;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Result;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.jdom.JDOMException;
import org.jdom.input.SAXBuilder;
import org.jdom.output.Format;
import org.jdom.output.XMLOutputter;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

public class Exb2EafConverter {

    public Exb2EafConverter() {
    }
    
    public static ArrayList<String> availableFor() {
        ArrayList<String> fileTypes = new ArrayList<String> ();
        fileTypes.add("exb");
        return fileTypes;
    }
    
    public Document runChecker(Document doc, String filename, Boolean fix, Properties properties) throws SAXException, IOException, 
                ParserConfigurationException, TransformerConfigurationException, TransformerException, XPathExpressionException, JDOMException {
        
        //preliminary manipulation: we need to set the ts tier as the main one for ELAM
        //tx is demoted to an annotation accordingly
        XPath xp = XPathFactory.newInstance().newXPath();
        String XPathTS = "//tier[@category='ts']";
        NodeList tstier = (NodeList) xp.compile(XPathTS).evaluate(doc, XPathConstants.NODESET);
        
        for (int i=0; i < tstier.getLength(); i++) {
            Element tier = (Element) tstier.item(i);
            tier.setAttribute("type", "t");
        }
        
        String XPathTX = "//tier[@category='tx']";
        NodeList txtier = (NodeList) xp.compile(XPathTX).evaluate(doc, XPathConstants.NODESET);
        
        for (int i=0; i < txtier.getLength(); i++) {
            Element tier = (Element) txtier.item(i);
            tier.setAttribute("type", "a");
        }
        
        //EAF files get broken if there are timeline items without a time attribute
        //so we need to make one, even for transcriptions without audio
        String XPathTLI = "//tli";
        NodeList allTlis = (NodeList) xp.compile(XPathTLI).evaluate(doc, XPathConstants.NODESET);
        //the following list is to get rid of such fake TLIs later on
        ArrayList<String> tlis2cleanup = new ArrayList();
        
        Float prevTime = Float.valueOf("0.0");
        
        for (int i=0; i < allTlis.getLength(); i++) {
            Element tli = (Element) allTlis.item(i);
            if (tli.hasAttribute("time")) {
                prevTime = Float.valueOf(tli.getAttribute("time"));
            } else {
                Float intpTime = Float.sum(prevTime, Float.parseFloat("1"));
                tli.setAttribute("time", intpTime.toString());
                tli.setAttribute("type", "intp");
                prevTime = intpTime;
                tlis2cleanup.add(tli.getAttribute("id"));
            }            
        }
        
        //then the manipulated EXB is saved so it can be accessed by the XSLTransformer
        //looks hacky, might be subject to change in the future
        URL docURL = new URL(doc.getBaseURI());
        File exbModified = new File(docURL.getFile());
        
        Transformer transformer = TransformerFactory.newInstance().newTransformer();
        Result output = new StreamResult(exbModified);
        Source input = new DOMSource(doc);
        transformer.transform(input, output);
        org.jdom.Document sf = new SAXBuilder().build(exbModified);
        Format prettyFormat = Format.getPrettyFormat();
        prettyFormat.setTextMode(Format.TextMode.TRIM_FULL_WHITE);
        prettyFormat.setOmitDeclaration(false); 
        FileOutputStream fos = new FileOutputStream(exbModified);
        XMLOutputter xmlOutputter = new XMLOutputter(prettyFormat);
        xmlOutputter.output(sf, fos);
        fos.close();
        
        String xslpath = "/xsl/BasicTranscription2EAF.xsl";    
        String xsl;
        
        try {
            String xml = FileUtils.readFileToString(exbModified, "utf-8");
      
            // get the XSLT stylesheet as String            
            java.util.Scanner s = new java.util.Scanner(getClass().getResourceAsStream(xslpath)).useDelimiter("\\A");
            xsl = s.hasNext() ? s.next() : "";
            
            // create XSLTransformer and set the parameters 
            XSLTransformer xt = new XSLTransformer();
            // perform XSLT transformation            
            String result = xt.transform(xml, xsl);
            //get location to save new result
            String outfile = FilenameUtils.getFullPath(doc.getBaseURI()) + filename.substring(0, filename.lastIndexOf(".")) + ".eaf";
            URL overviewurl = new URL(outfile);
            File outputfile = new File(overviewurl.getFile());
            
            try (Writer writer = new OutputStreamWriter(new FileOutputStream(outputfile), StandardCharsets.UTF_8)) {
                writer.write(result);
            }

        } catch (TransformerConfigurationException ex) {
            System.out.println("Transformer configuration error");
        } catch (TransformerException ex) {
            System.out.println("Transformer error");
        } catch (MalformedURLException ex) {
            System.out.println("Malformed URL error");
        } catch (IOException ex) {
            System.out.println("Unknown input/output error");
        } catch (Exception e) {
            System.out.println("Caught " + e);
        }
        
        //and now reverting the changes in the EXB 
        
        for (int i=0; i < tstier.getLength(); i++) {
            Element tier = (Element) tstier.item(i);
            tier.setAttribute("type", "a");
        }
        
        for (int i=0; i < txtier.getLength(); i++) {
            Element tier = (Element) txtier.item(i);
            tier.setAttribute("type", "t");
        }
        
        for (int i=0; i < allTlis.getLength(); i++) {
            Element tli = (Element) allTlis.item(i);
            if (tlis2cleanup.contains(tli.getAttribute("id"))) {
                tli.removeAttribute("time");
                tli.removeAttribute("type");
            }
        }
        
        //transformer.transform(input, output);
        //prettyFormat.setTextMode(Format.TextMode.TRIM_FULL_WHITE);
        //prettyFormat.setOmitDeclaration(false); 
        //xmlOutputter.output(sf, fos);
        //fos.close();
        
        return doc;
    }      
}
