package INEL.utilities;

import INEL.ReportItem;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Properties;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.xml.parsers.ParserConfigurationException;
import org.jdom.JDOMException;
import org.w3c.dom.Document;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

public class ExbFillInEvents {
    
    /*
    *   Required parameters: 
    *       "insert_tier" - tier category to insert new events to
    *       "insert_value" - the string to be inserted
    *       "lookup_tier" - tier category to check for the context
    *       "lookup_prefix" - regEx for the left context, will not be replaced
    *       "lookup_value" - regEx that should be matched
    *       "lookup_suffix"- regEx for the right context, will not be replaced
    *
    *   Optional parameters (when context from other tiers is needed):
    *       "context_tier" - tier category where we need to look up the context
    *       "context_value" - string that we try to match (if match, carry on the replacement, if not, do nothing)
    *       "context_prefix" - regEx for the left context of context_value
    *       "context_suffix" - regEx for the right context of context_value
    *
    *       "append" - a toggle between ignoring the existing events or appending insert_value to their text content (defult behavior - ignore)
    */

    public ExbFillInEvents() {
    }
    
    public static ArrayList<String> availableFor() {
        ArrayList<String> fileTypes = new ArrayList<String> ();
        fileTypes.add("exb");
        return fileTypes;
    }
    
    public Document runChecker(Document doc, String filename, Boolean fix, Properties properties) throws SAXException, IOException, ParserConfigurationException, JDOMException, XPathExpressionException {
        
       String newEventTier = null;
       String newGloss = null;
       String lookupTier = null;
       String lookupPrefix = null;
       String lookupValue = null;
       String lookupSuffix = null;
       String contextTier = null;
       String contextPrefix = null;
       String contextValue = null;
       String contextSuffix = null;
       Boolean appendIfExists = false;
        
        try {
            newEventTier = properties.get("insert_tier").toString();
            newGloss = properties.get("insert_value").toString();
            lookupTier = properties.get("lookup_tier").toString();
            lookupPrefix = properties.get("lookup_prefix").toString();
            lookupValue = properties.get("lookup_value").toString();
            lookupSuffix = properties.get("lookup_suffix").toString();
        } catch (NullPointerException npe) {
            //do nothing and print this message in case of failure to find some of the properties
            System.out.println("DEBUG MESSAGE: Invalid list of parameters for " + this.getClass().getSimpleName() + ", aborting...");
            return doc;
        }
        
        if (properties.containsKey("context_value")) {
            try {
                contextTier = properties.get("context_tier").toString();
                contextPrefix = properties.get("context_prefix").toString();
                contextValue = properties.get("context_value").toString();
                contextSuffix = properties.get("context_suffix").toString();
            } catch (NullPointerException npe) {
                //do nothing and print this message in case of failure to find some of the properties
                System.out.println("DEBUG MESSAGE: Invalid list of parameters for " + this.getClass().getSimpleName() + ", aborting...");
                return doc;
            }
        }
        
        if (properties.containsKey("append")) {
            appendIfExists = true;
        }
        
        XPath xp = XPathFactory.newInstance().newXPath();
        
        String patternRegex = lookupPrefix + lookupValue + lookupSuffix;
        Pattern glossFinder = Pattern.compile(patternRegex);
     
        String XPathGloss = "//tier[@category='" + lookupTier + "']/event";
        NodeList allGlosses = (NodeList) xp.compile(XPathGloss).evaluate(doc, XPathConstants.NODESET);

        for (int g = 0; g < allGlosses.getLength(); g++) {
            Object og = allGlosses.item(g);
            if (og instanceof Element) {
                Element matchedElement = (Element) og;
                String glossText = matchedElement.getTextContent();
                //System.out.println(glossText + "is the gloss text I've found");
                //System.out.println(glossFinder.toString() + "is the regular expression");
                Matcher m = glossFinder.matcher(glossText);
                if (m.find()) {
                    if (contextValue != null) {
                        //System.out.println("We have a match! Let's look for context... which should be " + contextValue);
                        String timeStamp = matchedElement.getAttribute("start");
                        String timeEnd = matchedElement.getAttribute("end");
                        Element parentTier = (Element) matchedElement.getParentNode();
                        String speaker = parentTier.getAttribute("speaker");
                        String XPathContext = "//tier[@category='" + contextTier + "'][@speaker='" + speaker + "']/event[@start='" + timeStamp + "']";
                        Node co = (Node) xp.compile(XPathContext).evaluate(doc, XPathConstants.NODE);
                        Element ce = (Element) co;
                        String contextText = ce.getTextContent();
                        String contextRegex = contextPrefix + contextValue + contextSuffix;
                        Pattern contextFinder = Pattern.compile(contextRegex);
                        //System.out.println(contextText + " is the context value");
                        //System.out.println(contextFinder.toString());
                        Matcher ctxm = contextFinder.matcher(contextText);
                        if (ctxm.find()) {
                            String XPathCheckEvent = "//tier[@category='" + newEventTier + "'][@speaker='" + speaker + "']/event[@start='" + timeStamp + "']";
                            try {
                                Node eo = (Node) xp.compile(XPathCheckEvent).evaluate(doc, XPathConstants.NODE);
                                Element ee = (Element) eo;
                                String curText = ee.getTextContent();
                                if (appendIfExists && fix) {
                                    //a switch to append or ignore the event is there is something present
                                    System.out.println(m.group() + " matched an expression you wanted to modify in " + glossText + " The context is " + contextText + 
                                            ". " + ee.getTextContent() + " already exists at " + timeStamp + ", I'll add more...");
                                    ee.setTextContent(curText + " " + newGloss);
                                    String message = "Appended to " + newGloss + ", tier is " + newEventTier + ", speaker is " + speaker + ". The lookup value is " + glossText + " at " + timeStamp;
                                    ReportItem.createReportItem(false, this.getClass().getName(), filename, message);
                                }
                            } catch (Exception e) {
                                if (fix) {
                                    System.out.println(m.group() + " matched an expression you wanted to modify in " + glossText + " The context is " + contextText);
                                    String XPathNewEvent = "//tier[@category='" + newEventTier + "'][@speaker='" + speaker + "']";
                                    Node neo = (Node) xp.compile(XPathNewEvent).evaluate(doc, XPathConstants.NODE);
                                    Element eeo = (Element) neo;
                                    Element newEvent = doc.createElement("event");
                                    newEvent.setAttribute("start", timeStamp);
                                    newEvent.setAttribute("end", timeEnd);
                                    newEvent.setTextContent(newGloss);
                                    eeo.appendChild(newEvent);
                                    
                                    String message = "Added " + newGloss + ", tier is " + newEventTier + ", speaker is " + speaker + ". The lookup value is " + glossText + " at " + timeStamp;
                                    ReportItem.createReportItem(false, this.getClass().getName(), filename, message);
                                }
                            }
                        } else {System.out.println("The context didn't match :(");}
                    } else {
                        //no context provided
                        String timeStamp = matchedElement.getAttribute("start");
                        String timeEnd = matchedElement.getAttribute("end");
                        Element parentTier = (Element) matchedElement.getParentNode();
                        String speaker = parentTier.getAttribute("speaker");
                        String XPathCheckEvent = "//tier[@category='" + newEventTier + "'][@speaker='" + speaker + "']/event[@start='" + timeStamp + "']";
                        try {
                            Node eo = (Node) xp.compile(XPathCheckEvent).evaluate(doc, XPathConstants.NODE);
                            Element ee = (Element) eo;
                            String curText = ee.getTextContent();
                            //System.out.println(ee.getTextContent() + " already exists at " + timeStamp + ",skipping...");
                            if (appendIfExists && fix) {
                                System.out.println(m.group() + " matched an expression you wanted to modify in " + glossText +  
                                            ". " + ee.getTextContent() + " already exists at " + timeStamp + ", I'll add more...");
                                ee.setTextContent(curText + " " + newGloss);
                                String message = "Appended " + newGloss + ", tier is " + newEventTier + ", speaker is " + speaker + ". The lookup value is " + glossText + " at " + timeStamp;
                                ReportItem.createReportItem(false, this.getClass().getName(), filename, message);
                            }
                        } catch (Exception e) {
                            if (fix) {
                                System.out.println(m.group() + " matched an expression you wanted to modify in " + glossText + " at " + timeStamp);
                                String XPathNewEvent = "//tier[@category='" + newEventTier + "'][@speaker='" + speaker + "']";
                                Node neo = (Node) xp.compile(XPathNewEvent).evaluate(doc, XPathConstants.NODE);
                                Element eeo = (Element) neo;
                                Element newEvent = doc.createElement("event");
                                newEvent.setAttribute("start", timeStamp);
                                newEvent.setAttribute("end", timeEnd);
                                newEvent.setTextContent(newGloss);
                                eeo.appendChild(newEvent);
                                
                                String message = "Added " + newGloss + ", tier is " + newEventTier + ", speaker is " + speaker + ". The lookup value is " + glossText + " at " + timeStamp;
                                ReportItem.createReportItem(false, this.getClass().getName(), filename, message);
                            }
                        }
                    }
                }
            }
        }
        return doc;
    }      
}
