package INEL.utilities;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Properties;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

public class ExbNormalizeTimeline {

    public ExbNormalizeTimeline() {
    }
    
    public static ArrayList<String> availableFor() {
        ArrayList<String> fileTypes = new ArrayList<> ();
        fileTypes.add("exb");
        return fileTypes;
    }
    
    public Document runChecker(Document doc, String filename, Boolean fix, Properties properties) throws SAXException, IOException, ParserConfigurationException, URISyntaxException,
            XPathExpressionException {
        
        if (fix) {
            NodeList timeline = doc.getElementsByTagName("tli");
            NodeList events = doc.getElementsByTagName("event");
            XPath xPath = XPathFactory.newInstance().newXPath();

            for (int i = 0; i < timeline.getLength(); i++) {
                Element tli = (Element) timeline.item(i);

                //normalize timeline

                String curTli = tli.getAttribute("id");
                
                if (!curTli.equals("T" + String.valueOf(i))) {
                    String xPathStart = "basic-transcription/basic-body/tier/event[@start='" + curTli + "']";
                    NodeList sameStart = (NodeList) xPath.compile(xPathStart).evaluate(doc, XPathConstants.NODESET);
                    for (int e = 0; e < sameStart.getLength(); e++) {
                        Element event = (Element) sameStart.item(e);
                        //"PT" is temporary notation and will be cleared up later in the cycle
                        event.setAttribute("start", "PT" + String.valueOf(i));
                    }

                    String xPathEnd = "basic-transcription/basic-body/tier/event[@end='" + curTli + "']";
                    NodeList sameEnd = (NodeList) xPath.compile(xPathEnd).evaluate(doc, XPathConstants.NODESET);
                    for (int e = 0; e < sameEnd.getLength(); e++) {
                        Element event = (Element) sameEnd.item(e);                   
                        //"PT" is temporary notation and will be cleared up later in the cycle
                        event.setAttribute("end", "PT" + String.valueOf(i));
                    } 
                    tli.setAttribute("id", "T" + String.valueOf(i));
                }
            }

            //clear up events

            for (int e = 0; e < events.getLength(); e++) {
                Element event = (Element) events.item(e);
                String curStart = event.getAttribute("start");
                String curEnd = event.getAttribute("end");

                if (curStart.startsWith("PT")) {
                    event.setAttribute("start", curStart.substring(1));
                }
                if (curEnd.startsWith("PT")) {
                    event.setAttribute("end", curEnd.substring(1));
                }
            }
        }
        System.out.println(filename + ": done normalizing the timeline");
        return doc;
    }      
}
