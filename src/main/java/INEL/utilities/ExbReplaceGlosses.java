package INEL.utilities;

import INEL.ReportItem;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Properties;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.xml.parsers.ParserConfigurationException;
import org.jdom.JDOMException;
import org.w3c.dom.Document;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

public class ExbReplaceGlosses {
    
    /*
    *   Required parameters: 
    *       "tier" - tier category where we want to perform a replacement
    *       "original" - string to be replaced
    *       "replacement_prefix" - regEx for the left context, will not be replaced
    *       "replacement_suffix"- regEx for the right context, will not be replaced
    *       "new"- string that will replace the original
    *
    *   Optional parameters (when context from other tiers is needed):
    *       "context_tier"- tier category where we need to look up the context
    *       "context_value" - string that we try to match (if match, carry on the replacement, if not, do nothing)
    *       "context_prefix"- regEx for the left context of context_value
    *       "context_suffix"= regEx for the right context of context_value
    */

    public ExbReplaceGlosses() {
    }
    
    public static ArrayList<String> availableFor() {
        ArrayList<String> fileTypes = new ArrayList<String> ();
        fileTypes.add("exb");
        return fileTypes;
    }
    
    public Document runChecker(Document doc, String filename, Boolean fix, Properties properties) throws SAXException, IOException, ParserConfigurationException, JDOMException, XPathExpressionException {
        
       String replacementTier = null;
       String replacementPrefix = null;
       String originalGloss = null;
       String replacementSuffix = null;
       String newGloss = null;
       String contextTier = null;
       String contextPrefix = null;
       String contextValue = null;
       String contextSuffix = null;
        
        try {
            replacementTier = properties.get("tier").toString();
            replacementPrefix = properties.get("replacement_prefix").toString();
            originalGloss = properties.get("original").toString();
            replacementSuffix = properties.get("replacement_suffix").toString();
            newGloss = properties.get("new").toString();
        } catch (NullPointerException npe) {
            //do nothing and print this message in case of failure to find some of the properties
            System.out.println("DEBUG MESSAGE: Invalid list of parameters for " + this.getClass().getSimpleName() + ", aborting...");
            return doc;
        }
        
        if (properties.containsKey("context_value")) {
            try {
                contextTier = properties.get("context_tier").toString();
                contextPrefix = properties.get("context_prefix").toString();
                contextValue = properties.get("context_value").toString();
                contextSuffix = properties.get("context_suffix").toString();
            } catch (NullPointerException npe) {
                //do nothing and print this message in case of failure to find some of the properties
                System.out.println("DEBUG MESSAGE: Invalid list of parameters for " + this.getClass().getSimpleName() + ", aborting...");
                return doc;
            }
        }
        
        XPath xp = XPathFactory.newInstance().newXPath();
        
        String patternRegex = replacementPrefix + originalGloss + replacementSuffix;
        Pattern glossFinder = Pattern.compile(patternRegex);
        String XPathGloss = "//tier[@category='" + replacementTier + "']/event";
        NodeList allGlosses = (NodeList) xp.compile(XPathGloss).evaluate(doc, XPathConstants.NODESET);
        for (int g = 0; g < allGlosses.getLength(); g++) {
            Object og = allGlosses.item(g);
            if (og instanceof Element) {
                Element matchedElement = (Element) og;
                String glossText = matchedElement.getTextContent();
                Matcher m = glossFinder.matcher(glossText);
                if (m.find()) {
                    if (contextValue != null) {
                        String timeStamp = matchedElement.getAttribute("start");
                        String XPathContext = "//tier[@category='" + contextTier + "']/event[@start='" + timeStamp + "']";
                        Node co = (Node) xp.compile(XPathContext).evaluate(doc, XPathConstants.NODE);
                        Element ce = (Element) co;
                        try {
                            String contextText = ce.getTextContent();
                            String contextRegex = contextPrefix + contextValue + contextSuffix;
                            Pattern contextFinder = Pattern.compile(contextRegex);
                            Matcher ctxm = contextFinder.matcher(contextText);
                            if (ctxm.find()) {
                                System.out.println(m.group() + " matched an expression you wanted to replace in " + glossText + " The context is " + contextText);
                                glossText = m.replaceAll(newGloss);
                                matchedElement.setTextContent(glossText);
                                if (fix) {
                                    String message = "Replaced " + originalGloss + " with " + newGloss + " in " + glossText;
                                    ReportItem.createReportItem(false, this.getClass().getName(), filename, message);
                                }
                            } else {System.out.println("The context didn't match :(");}
                        } catch (NullPointerException npe) {
                            System.out.println("No annotation found in the context tier, skipping...");
                        }
                    } else {
                        System.out.println(m.group() + " matched an expression you wanted to replace in " + glossText);
                        glossText = m.replaceAll(newGloss);
                        matchedElement.setTextContent(glossText);
                        if (fix) {
                            String message = "Replaced " + originalGloss + " with " + newGloss + " in " + glossText;
                            ReportItem.createReportItem(false, this.getClass().getName(), filename, message);
                        }
                    }
                }
            }
        }
        return doc;
    }      
}
