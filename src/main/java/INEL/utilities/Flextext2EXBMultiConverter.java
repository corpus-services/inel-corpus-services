package INEL.utilities;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Properties;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;

public class Flextext2EXBMultiConverter {

    public Flextext2EXBMultiConverter() {
    }
    
    public static ArrayList<String> availableFor() {
        ArrayList<String> fileTypes = new ArrayList<String> ();
        fileTypes.add("flextext");
        return fileTypes;
    }
    
    public Document runChecker(Document doc, String filename, Boolean fix, Properties properties) throws SAXException, IOException, ParserConfigurationException, URISyntaxException {
        
        String settingsPath = null;
        
        try {
            //the path to a corpus-specific file, e.g. corpusname_flextext2exb.xsl
            settingsPath = properties.get("settings_path").toString();
        } catch (NullPointerException npe) {
            //do nothing and print this message in case of failure to find some of the properties
            System.out.println("DEBUG MESSAGE: Invalid list of parameters for " + this.getClass().getSimpleName() + ", aborting...");
            return doc;
        }
        
        Path fpath = Paths.get(settingsPath).toAbsolutePath();
        File settingsFile = new File(fpath.toUri());
        
        String xslpath = "/xsl/flextext2exb-multi.xsl";    
        String xsl;
        
        try {
            URL docURL = new URL(doc.getBaseURI());
            File flextext = new File(docURL.getFile());
            String xml = FileUtils.readFileToString(flextext, "utf-8");
      
            // get the XSLT stylesheet as String            
            java.util.Scanner s = new java.util.Scanner(getClass().getResourceAsStream(xslpath)).useDelimiter("\\A");
            xsl = s.hasNext() ? s.next() : "";
            
            // create XSLTransformer and set the parameters 
            XSLTransformer xt = new XSLTransformer();
            
            xt.setParameter("SETTINGS-FILE", settingsFile);
            xt.setParameter("baseURL", docURL.toString().substring(0, docURL.toString().lastIndexOf("/")));
            // perform XSLT transformation
            
            String result = xt.transform(xml, xsl);
            //get location to save new result
            String outfile = FilenameUtils.getFullPath(doc.getBaseURI()) + "test.xml";
            URL overviewurl = new URL(outfile);
            File outputfile = new File(overviewurl.getFile());
            
            try (Writer writer = new OutputStreamWriter(new FileOutputStream(outputfile), StandardCharsets.UTF_8)) {
                writer.write(result);
            }

        } catch (TransformerConfigurationException ex) {
            System.out.println("Transformer configuration error");
        } catch (TransformerException ex) {
            System.out.println("Transformer error");
        } catch (MalformedURLException ex) {
            System.out.println("Malformed URL error");
        } catch (IOException ex) {
            System.out.println("Unknown input/output error");
        } catch (Exception e) {
            System.out.println("Caught " + e);
        } 
              
        return doc;
    }      
}
