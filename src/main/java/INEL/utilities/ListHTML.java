package INEL.utilities;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Properties;
import java.util.regex.Pattern;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.exmaralda.partitureditor.jexmaralda.BasicTranscription;
import org.exmaralda.partitureditor.jexmaralda.ListTranscription;
import org.exmaralda.partitureditor.jexmaralda.segment.HIATSegmentation;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;

public class ListHTML {

    public ListHTML() {
    }

    public static ArrayList<String> availableFor() {
        ArrayList<String> fileTypes = new ArrayList<String>();
        fileTypes.add("exb");
        return fileTypes;
    }

    public Document runChecker(Document doc, String filename, Boolean fix, Properties properties) throws SAXException, IOException, ParserConfigurationException, URISyntaxException {

        String xslpath = "/xsl/HIAT2ListHTML.xsl";
        String xsl;
        String pathToExternalFSM = (String) properties.get("fsm");

        try {

            URL docURL = new URL(doc.getBaseURI());
            File exb = new File(docURL.getFile());
            String xml = FileUtils.readFileToString(exb, "utf-8");

            BasicTranscription bt;
            HIATSegmentation segmentation = new org.exmaralda.partitureditor.jexmaralda.segment.HIATSegmentation();

            if (properties.stringPropertyNames().contains("fsm")) {
                segmentation.pathToExternalFSM = properties.getProperty("fsm");
            }
            String list = null;
            org.exmaralda.partitureditor.jexmaralda.sax.BasicTranscriptionSaxReader reader = new org.exmaralda.partitureditor.jexmaralda.sax.BasicTranscriptionSaxReader();
            bt = reader.readFromFile(exb.getAbsolutePath());
            ListTranscription lt = segmentation.BasicToUtteranceList(bt);
            // get the XSLT stylesheet as String            
            java.util.Scanner s = new java.util.Scanner(getClass().getResourceAsStream(xslpath)).useDelimiter("\\A");
            xsl = s.hasNext() ? s.next() : "";

            // create XSLTransformer and set the parameters 
            XSLTransformer xt = new XSLTransformer();

            // perform XSLT transformation
            String result = xt.transform(lt.toXML(), xsl);
            
            

            //get location to save new result
            String outfile = FilenameUtils.getFullPath(doc.getBaseURI()) + filename.substring(0, filename.lastIndexOf(".")) + "_list.html";
            URL overviewurl = new URL(outfile);
            File outputfile = new File(overviewurl.getFile());
            
            

            try (Writer writer = new OutputStreamWriter(new FileOutputStream(outputfile), StandardCharsets.UTF_8)) {
                writer.write(result);
            }

        } catch (TransformerConfigurationException ex) {
            System.out.println("Transformer configuration error");
        } catch (TransformerException ex) {
            System.out.println("Transformer error");
        } catch (MalformedURLException ex) {
            System.out.println("Malformed URL error");
        } catch (IOException ex) {
            System.out.println("Unknown input/output error");
        } catch (Exception e) {
            System.out.println("Caught " + e);
        }

        return doc;
    }

}
