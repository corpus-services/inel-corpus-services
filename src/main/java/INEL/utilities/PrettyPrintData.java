package INEL.utilities;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Properties;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;

public class PrettyPrintData {

    public PrettyPrintData() {
    }
    
    public static ArrayList<String> availableFor() {
        ArrayList<String> fileTypes = new ArrayList<> ();
        fileTypes.add("exb");
        //fileTypes.add("exs");
        fileTypes.add("coma");
        return fileTypes;
    }
    
    public Document runChecker(Document doc, String filename, Boolean fix, Properties properties) throws SAXException, IOException, ParserConfigurationException, URISyntaxException {
        
        //dummy function - does nothing by itself, but will mark the files for pretty printing via CorpusFunctions
        
        return doc;
    }      
}
