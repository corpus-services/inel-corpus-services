package INEL.utilities;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;
import java.util.Vector;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import org.apache.commons.io.FilenameUtils;
import org.exmaralda.common.corpusbuild.FileIO;
import org.exmaralda.partitureditor.interlinearText.HTMLParameters;
import org.exmaralda.partitureditor.interlinearText.InterlinearText;
import org.exmaralda.partitureditor.jexmaralda.BasicTranscription;
import org.exmaralda.partitureditor.jexmaralda.TierFormatTable;
import org.exmaralda.partitureditor.jexmaralda.convert.ItConverter;
import org.jdom.Element;
import org.jdom.filter.ElementFilter;
import org.jdom.output.XMLOutputter;
import org.jdom.xpath.XPath;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;

public class ScoreHTML {

    private Integer width = 900;
    private String html = null;

    public ScoreHTML() {
    }

    public static ArrayList<String> availableFor() {
        ArrayList<String> fileTypes = new ArrayList<String>();
        fileTypes.add("exb");
        return fileTypes;
    }

    public Document runChecker(Document doc, String filename, Boolean fix, Properties properties) throws SAXException, IOException, ParserConfigurationException, URISyntaxException {

        String xslpath = "/xsl/Score2HTML.xsl";
        String xsl;

        try {

            URL docURL = new URL(doc.getBaseURI());
            File exb = new File(docURL.getFile());

            BasicTranscription bt;

            org.exmaralda.partitureditor.jexmaralda.sax.BasicTranscriptionSaxReader reader = new org.exmaralda.partitureditor.jexmaralda.sax.BasicTranscriptionSaxReader();
            bt = reader.readFromFile(exb.getAbsolutePath());

            bt.normalize();
            TierFormatTable tft = new TierFormatTable(bt);

            ItConverter itc = new ItConverter();

            InterlinearText it = itc.BasicTranscriptionToInterlinearText(bt, tft, 0);


            HTMLParameters param = new HTMLParameters();
            param.setWidth(width);
            param.stretchFactor = 1.2;
            param.smoothRightBoundary = true;
            param.includeSyncPoints = true;
            param.putSyncPointsOutside = true;
            param.outputAnchors = true;
            param.frame = "lrtb";
            param.frameStyle = "Solid";
            param.setFrameColor(new java.awt.Color(153, 153, 153));

            it.trim(param);

            String itAsString = it.toXML();
            String styles = "/* EMTPY TIER FORMAT TABLE!!! */";
            if (bt.getTierFormatTable() != null) {
                styles = bt.getTierFormatTable().toTDCSS();
            }
            final org.jdom.Document itDocument = FileIO.readDocumentFromString(itAsString);
            org.jdom.Document btDocument = bt.toJDOMDocument();
            Iterator i = itDocument.getRootElement().getDescendants(new ElementFilter("line"));
            Vector toBeRemoved = new Vector();
            while (i.hasNext()) {
                toBeRemoved.addElement(i.next());
            }
            for (int pos = 0; pos < toBeRemoved.size(); pos++) {
                Element e = (Element) (toBeRemoved.elementAt(pos));
                e.detach();
            }
            XPath xpath1 = XPath.newInstance("//common-timeline");
            Element timeline = (Element) (xpath1.selectSingleNode(btDocument));
            timeline.detach();

            XPath xpath2 = XPath.newInstance("//head");
            Element head = (Element) (xpath2.selectSingleNode(btDocument));
            head.detach();

            XPath xpath3 = XPath.newInstance("//tier");
            List tiers = xpath3.selectNodes(btDocument);
            Element tiersElement = new Element("tiers");
            for (int pos = 0; pos < tiers.size(); pos++) {
                Element t = (Element) (tiers.get(pos));
                t.detach();
                t.removeContent();
                tiersElement.addContent(t);
            }

            Element tableWidthElement = new Element("table-width");
            tableWidthElement.setAttribute("table-width", Long.toString(Math.round(param.getWidth())));

            Element btElement = new Element("basic-transcription");
            //            btElement.addContent(nameElement);
            btElement.addContent(tableWidthElement);
            btElement.addContent(head);
            btElement.addContent(timeline);
            btElement.addContent(tiersElement);

            itDocument.getRootElement().addContent(btElement);

            XMLOutputter xmOut = new XMLOutputter();
            String xml = xmOut.outputString(itDocument);

            // get the XSLT stylesheet as String            
            java.util.Scanner s = new java.util.Scanner(getClass().getResourceAsStream(xslpath)).useDelimiter("\\A");
            xsl = s.hasNext() ? s.next() : "";

            // create XSLTransformer and set the parameters 
            XSLTransformer xt = new XSLTransformer();

            // perform XSLT transformation
            String result = xt.transform(xml, xsl);


            setHTML(result);

            //get location to save new result
            String outfile = FilenameUtils.getFullPath(doc.getBaseURI()) + filename.substring(0, filename.lastIndexOf(".")) + "_score.html";
            URL overviewurl = new URL(outfile);
            File outputfile = new File(overviewurl.getFile());

            try (Writer writer = new OutputStreamWriter(new FileOutputStream(outputfile), StandardCharsets.UTF_8)) {
                writer.write(result);
            }

        } catch (TransformerConfigurationException ex) {
            System.out.println("Transformer configuration error");
        } catch (TransformerException ex) {
            System.out.println("Transformer error");
        } catch (MalformedURLException ex) {
            System.out.println("Malformed URL error");
        } catch (IOException ex) {
            System.out.println("Unknown input/output error");
        } catch (Exception e) {
            System.out.println("Caught " + e);
            e.printStackTrace();
        }

        return doc;
    }

    public Integer getWidth() {
        return width;
    }

    public void setWidth(Integer width) {
        this.width = width;
    }

    public void setHTML(String c) {
        html = c;
    }

}
