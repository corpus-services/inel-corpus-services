package INEL.utilities;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import net.sf.saxon.s9api.Processor;
import net.sf.saxon.s9api.SaxonApiException;
import net.sf.saxon.s9api.XQueryCompiler;
import net.sf.saxon.s9api.XQueryEvaluator;
import net.sf.saxon.s9api.XQueryExecutable;
import net.sf.saxon.s9api.XdmItem;
import net.sf.saxon.s9api.XdmValue;
import org.apache.commons.io.FileUtils;

public class XQueryWrapper {
    
    String xqresource;
    
    public void run(File inputdir, String queryname) throws MalformedURLException, SaxonApiException, IOException {
    
        try {
            
            if (queryname.endsWith(".xquery")) {
                xqresource = "/xquery/" + queryname;
            } else {
                xqresource = "/xquery/" + queryname + ".xquery";
            }
            
            String xqString;
            java.util.Scanner scan = new java.util.Scanner(getClass().getResourceAsStream(xqresource)).useDelimiter("\\A");
            xqString = scan.hasNext() ? scan.next() : "";
            
            /*URL resURL = XQueryWrapper.class.getResource(xqresource);
            File xqueryFile = new File(resURL.getFile());
            String xqString = FileUtils.readFileToString(xqueryFile, "utf-8");*/

            Processor proc = new Processor(false);
            XQueryCompiler comp = proc.newXQueryCompiler();
            //set a path
            comp.setBaseURI(inputdir.toURI());
            XQueryExecutable exp = comp.compile(xqString);
            XQueryEvaluator qe = exp.load();
            XdmValue result = qe.evaluate();
            StringBuilder output = new StringBuilder();
            for (XdmItem item : result) {
                output.append(item.getStringValue());
            }

            LocalDateTime dateRaw = LocalDateTime.now();
            DateTimeFormatter dateFormat = DateTimeFormatter.ofPattern("yyyy-mm-dd-HH-mm-ss");
            String date = dateRaw.format(dateFormat);
            
            String outputfname = inputdir.getAbsolutePath() + "/" + queryname + "-" + date + ".txt";
            URL outputsingleurl = Paths.get(outputfname).toUri().toURL();
            File outputfile = new File(outputsingleurl.getFile());

            try (Writer writer = new OutputStreamWriter(new FileOutputStream(outputfile), StandardCharsets.UTF_8)) {

                writer.write(output.toString());
            }
        } catch (Exception e) {
            System.out.println(e);
        }
    } 
}
