package INEL.utilities;

import java.io.*;
import java.nio.file.*;
import java.util.Properties;
import java.util.regex.Pattern;
import java.util.zip.*;

public class ZipCorpus {

    public void zip(File inputDir, Properties properties) throws IOException {
        // Specify the output ZIP file name
        String corpusVersion = (String) properties.get("version");
        String zipFileName = inputDir.getAbsolutePath() + "-" + corpusVersion + ".zip";
        if (properties.contains("mp3")) {
            zipFileName = inputDir.getAbsolutePath() + "-" + corpusVersion + "-mp3only.zip";
        } else if (properties.contains("none")) {
            zipFileName = inputDir.getAbsolutePath() + "-" + corpusVersion + "-noaudio.zip";
        }

        try (ZipOutputStream zos = new ZipOutputStream(new FileOutputStream(zipFileName))) {
            if (properties.containsValue("all")) {
                Pattern all = Pattern.compile("exb|exs|coma|pdf|wav|mp3|eaf|tei\\.xml");

                Path dir = inputDir.toPath();
                Files.walk(dir).forEach(path -> {
                    File file = path.toFile();
                    if (!file.isDirectory() && !file.getAbsolutePath().contains(File.separator + ".git" + File.separator)) {
                        String fileName = file.getName();
                        String extension = fileName.substring(fileName.lastIndexOf(".") + 1);
                        int teiIndex = fileName.lastIndexOf("tei");
                        String extensionTEI = (teiIndex != -1) ? fileName.substring(teiIndex - 1) : "";
                        if (all.matcher(extension).find() || all.matcher(extensionTEI).find()) {
                            try {
                                addToZip(dir, file, zos);
                                System.out.println("Now adding " + fileName);
                            } catch (IOException e) {
                            }
                        }
                    }
                });
                System.out.println("ZIP file created successfully!");
            }
            if (properties.containsValue("mp3")) {
                Pattern all = Pattern.compile("exb|exs|coma|pdf|mp3|eaf|tei\\.xml");

                Path dir = inputDir.toPath();
                Files.walk(dir).forEach(path -> {
                    File file = path.toFile();
                    if (!file.isDirectory() && !file.getAbsolutePath().contains(File.separator + ".git" + File.separator)) {
                        String fileName = file.getName();
                        String extension = fileName.substring(fileName.lastIndexOf(".") + 1);
                        int teiIndex = fileName.lastIndexOf("tei");
                        String extensionTEI = (teiIndex != -1) ? fileName.substring(teiIndex - 1) : "";
                        if (all.matcher(extension).find() || all.matcher(extensionTEI).find()) {
                            try {
                                addToZip(dir, file, zos);
                                System.out.println("Now adding " + fileName);
                            } catch (IOException e) {
                            }
                        }
                    }
                });
                System.out.println("ZIP file created successfully!");
            }
            if (properties.containsValue("none")) {
                Pattern all = Pattern.compile("exb|exs|coma|pdf|eaf|tei\\.xml");

                Path dir = inputDir.toPath();
                Files.walk(dir).forEach(path -> {
                    File file = path.toFile();
                    if (!file.isDirectory() && !file.getAbsolutePath().contains(File.separator + ".git" + File.separator)) {
                        String fileName = file.getName();
                        String extension = fileName.substring(fileName.lastIndexOf(".") + 1);
                        int teiIndex = fileName.lastIndexOf("tei");
                        String extensionTEI = (teiIndex != -1) ? fileName.substring(teiIndex - 1) : "";
                        if (all.matcher(extension).find() || all.matcher(extensionTEI).find()) {
                            try {
                                addToZip(dir, file, zos);
                                System.out.println("Now adding " + fileName);
                            } catch (IOException e) {
                            }
                        }
                    }
                });
                System.out.println("ZIP file created successfully!");
            }
        }
    }

    private void addToZip(Path inputDir, File file, ZipOutputStream zos) throws IOException {
        Path filePath = inputDir.relativize(file.toPath());
        String entryName = filePath.toString().replace(File.separator, "/");

        if (file.isDirectory()) {
            entryName += "/";
        }

        ZipEntry zipEntry = new ZipEntry(entryName);
        zos.putNextEntry(zipEntry);

        if (!file.isDirectory()) {
            try (FileInputStream fis = new FileInputStream(file)) {
                byte[] buffer = new byte[1024];
                int bytesRead;
                while ((bytesRead = fis.read(buffer)) != -1) {
                    zos.write(buffer, 0, bytesRead);
                }
            }
        }

        zos.closeEntry();

    }
}
