xquery version "3.1";
declare namespace output="http://www.w3.org/2010/xslt-xquery-serialization";
declare option output:omit-xml-declaration "yes";
declare option output:method "text";
declare option output:item-separator "";

declare variable $parameters := 'parameters-pos.xml';
(: location of the external file with parameters :)
(: expected in the same directory as the query :)


(: List all part-of-speech tags in a corpus :)

let 
   $corpusroot := if(doc($parameters)//corpus-root/@value) 
      then doc($parameters)//corpus-root/@value 
      else 'file:/C:/LANGS-Corpora/',
   $corpora := if(doc($parameters)//corpora/corpus/@value) 
      then doc($parameters)//corpora/corpus/@value 
      else ('SelkupDFGxCorpus','SelkupCorpus'),
   $tier := ('ps')

return(
   string-join(('pos','count','corpus'),'&#x09;')||'&#x0A;',
   (: write table header :)

for $c in $corpora
   (: cycle through corpora  :)
   let $coll := collection($corpusroot||$c||'/?select=*.exb;recurse=yes'),
   $items := $coll//tier[@category=$tier]/event/text(),
   $types := distinct-values($items)
   
   for $t in $types order by $t,$c return string-join(($t,count($items[.=$t]),$c),'&#x09;')||'&#x0A;'
   (: output as tab-separated text, a line per gloss, with counts (per corpus and tier)  :)
)