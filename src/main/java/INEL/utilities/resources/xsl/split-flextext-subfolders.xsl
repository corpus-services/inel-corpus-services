<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:my="http://www.philol.msu.ru/~languedoc/xml"
    exclude-result-prefixes="#all" version="2.0">

    <xsl:param name="baseURL"/>
    <xsl:output method="xml" indent="yes" encoding="utf-8" omit-xml-declaration="no"/>
    <xsl:namespace-alias stylesheet-prefix="#default" result-prefix=""/>

    <xsl:template match="/">
        <!--interlinear-split-report.xml-->
        <xsl:result-document method="xml" indent="yes" encoding="utf-8" omit-xml-declaration="no"
            href="{concat($baseURL,'/flextext-split-output/flextext-split-report-',format-dateTime(current-dateTime(),'[Y][M01][D01]-[H01][m][s]'),'.xml')}">
            <conversion-report>
                <input-filename>
                    <xsl:value-of select="$baseURL"/>
                </input-filename>
                <xsl:apply-templates select="//interlinear-text"/>
            </conversion-report>
        </xsl:result-document>
    </xsl:template>

    <xsl:template match="//interlinear-text">

        <xsl:variable name="textname"
            select="current()/item[@type = 'title-abbreviation' and @lang = 'en']/text()"/>

        <interlinear-text name="{$textname}"/>
        <xsl:result-document method="xml" indent="yes" encoding="utf-8" omit-xml-declaration="no"
            href="{concat($baseURL,'/flextext-split-output/',$textname,'/',$textname,'.flextext')}">
            <document version="2">
                <interlinear-text name="{$textname}">
                    <xsl:apply-templates/>
                </interlinear-text>
            </document>
        </xsl:result-document>
    </xsl:template>

    <!-- identity template -->
    <xsl:template match="node()">
        <xsl:copy>
            <xsl:copy-of select="@*"/>
            <xsl:apply-templates/>
        </xsl:copy>
    </xsl:template>

</xsl:stylesheet>
