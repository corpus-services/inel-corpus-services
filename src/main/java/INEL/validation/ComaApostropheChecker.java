package INEL.validation;

import INEL.ReportItem;
import java.io.File;
import java.io.IOException;
import java.io.StringReader;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Properties;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.apache.commons.io.FileUtils;
import org.w3c.dom.Document;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

public class ComaApostropheChecker {

    public ComaApostropheChecker() {
    }
    
    public static ArrayList<String> availableFor() {
        ArrayList<String> fileTypes = new ArrayList<String> ();
        fileTypes.add("coma");
        return fileTypes;
    }
    
    public Document runChecker(Document doc, String filename, Boolean fix, Properties properties) throws SAXException, IOException, ParserConfigurationException, URISyntaxException {
        
        URL docurl = new URL(doc.getDocumentURI());
        File docfile = new File(docurl.toURI());
        
        String docText = FileUtils.readFileToString(docfile, "utf-8");
        if (docText.contains("'")) {
            if (fix) {
                docText = docText.replaceAll("'", "’");
                DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
                DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
                InputSource is = new InputSource(new StringReader(docText));
                doc = dBuilder.parse(is);
                doc.getDocumentElement().normalize();
                //System.out.println(doc.getElementsByTagName("Corpus").item(0).getNodeName());
            } else {
                String message = "Comafile contains apostrophes (')";
                ReportItem.createReportItem(true, this.getClass().getSimpleName(), filename, message);
                System.out.println(message);
            }
        } 
        return doc;
    }
       
}