package INEL.validation;

import INEL.ReportItem;
import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Properties;
import java.util.regex.Pattern;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

public class ComaAttachedFilepathsChecker {

    public ComaAttachedFilepathsChecker() {
    }

    public static ArrayList<String> availableFor() {
        ArrayList<String> fileTypes = new ArrayList<String>();
        fileTypes.add("coma");
        return fileTypes;
    }

    public Document runChecker(Document doc, String filename, Boolean fix, Properties properties) throws SAXException, IOException, ParserConfigurationException, URISyntaxException {
        String baseURI = doc.getBaseURI();
        String basePath = "";
        if (baseURI.contains("/")) { //Linux path
            basePath = doc.getBaseURI().substring(0, doc.getBaseURI().lastIndexOf("/") + 1);
        } else if (baseURI.contains("\\")) { //Win path
            basePath = doc.getBaseURI().substring(0, doc.getBaseURI().lastIndexOf("\\") + 1);
        }

        Pattern absolutePathPattern = Pattern.compile("^(file:[/\\\\]+)?[A-Za-z]:");
        NodeList relPaths = doc.getElementsByTagName("relPath");

        for (int i = 0; i < relPaths.getLength(); i++) {
            Element relPath = (Element) relPaths.item(i);
            String relPathText = relPath.getTextContent();

            if (absolutePathPattern.matcher(relPathText).find()) {
                String message = "relPath " + relPathText + " appears to be an absolute path.";
                ReportItem.createReportItem(true, this.getClass().getSimpleName(), filename, message);
            }

            URL fileURL = new URL(basePath + relPathText);
            File referencedFile = new File(fileURL.getFile());
            if (!referencedFile.exists()) {
                String message = "File in relPath " + relPathText + " not found.";
                ReportItem.createReportItem(true, this.getClass().getSimpleName(), filename, message);

            }
        }

        NodeList absPaths = doc.getElementsByTagName("absPath");

        for (int i = 0; i < absPaths.getLength(); i++) {
            Element absPath = (Element) absPaths.item(i);
            String absPathText = absPath.getTextContent();

            if (absolutePathPattern.matcher(absPathText).find()) {
                if (fix) {
                    //String pattern = "(.*\\\\)?(?=nar|flk|flks|flkd|sng|transl|misc\\/\\\\\\\\)";
                    //System.out.println(pattern);
                    //absPathText = absPathText.replaceAll(pattern, ""); 
                    //absPath.setTextContent(absPathText);
                    String language = filename.substring(0, filename.lastIndexOf("."));
                    String langCorpus = language.substring(0, 1).toUpperCase() + language.substring(1) + "Corpus";
                    String dirName = ".+[/\\\\](" + language + "|" + langCorpus + ")[/\\\\]";
                    absPathText = absPathText.replaceAll(dirName, "");
                    absPath.setTextContent(absPathText);
                } else {
                    String message = "absPath " + absPathText + " appears to be an absolute path.";
                    ReportItem.createReportItem(true, this.getClass().getSimpleName(), filename, message);
                }
            }
        }
        return doc;

    }
}
