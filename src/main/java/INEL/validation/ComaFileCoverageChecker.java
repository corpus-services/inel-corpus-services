package INEL.validation;

import INEL.ReportItem;
import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Properties;
import java.util.UUID;
import java.util.regex.Pattern;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

public class ComaFileCoverageChecker {

    public ComaFileCoverageChecker() {
    }
    
    public static ArrayList<String> availableFor() {
        ArrayList<String> fileTypes = new ArrayList<String> ();
        fileTypes.add("coma");
        return fileTypes;
    }
    
    public Document runChecker(Document doc, String filename, Boolean fix, Properties properties) throws SAXException, IOException, ParserConfigurationException, URISyntaxException {
        
        String baseURI = doc.getBaseURI();
        String basePath = "";
        if (baseURI.contains("/")) { //Linux path
            basePath = doc.getBaseURI().substring(0, doc.getBaseURI().lastIndexOf("/") + 1);
        } else if (baseURI.contains("\\")) { //Win path
            basePath = doc.getBaseURI().substring(0, doc.getBaseURI().lastIndexOf("\\") + 1);
        }
        
        ArrayList<String> allRefsInComa = new ArrayList<>();
        
        NodeList NSLinks = doc.getElementsByTagName("NSLink");
        for (int i = 0; i < NSLinks.getLength(); i++) {
            allRefsInComa.add(NSLinks.item(i).getTextContent());
        }
        
        NodeList relPaths = doc.getElementsByTagName("relPath");
        for (int i = 0; i < relPaths.getLength(); i++) {
            allRefsInComa.add(relPaths.item(i).getTextContent());
        }   
        
        URL pathURL = new URL(basePath);        
        Path corpDir = Paths.get(pathURL.toURI());
        Pattern ignoreThose = Pattern.compile("\\.git|README|Thumbs\\.db|curation|resources|metadata|corpus\\-utilities|corpus\\-materials|SegmentationErrors|\\.coma");
                
        Files.walk(corpDir).forEach(path -> {
            if (!ignoreThose.matcher(path.toFile().toString()).find()) {
                if (!path.toFile().isDirectory()) {
                    String fileRelPath = path.toFile().toString().replace(corpDir.toString(), "");
                    if (!allRefsInComa.contains(fileRelPath.substring(1))) {
                        if (fileRelPath.endsWith(".exs")) {
                            String fileName = fileRelPath.substring(fileRelPath.lastIndexOf("/") + 1);
                            String communicationName = fileRelPath.substring(fileRelPath.lastIndexOf("/") + 1, fileRelPath.lastIndexOf("_"));
                            XPath xp = XPathFactory.newInstance().newXPath();
                            String XPathContext = "//Communication[@Name='" + communicationName + "']";
                            Node co;
                            try {
                                co = (Node) xp.compile(XPathContext).evaluate(doc, XPathConstants.NODE);
                                Element ce = (Element) co;
                                
                                Element newTranscription = doc.createElement("Transcription");
                                newTranscription.setAttribute("Id", UUID.randomUUID().toString().toUpperCase());
                                
                                Element trName = doc.createElement("Name");
                                trName.setTextContent(communicationName);
                                newTranscription.appendChild(trName);
                                
                                Element trFilename = doc.createElement("Filename");
                                trFilename.setTextContent(fileName);
                                newTranscription.appendChild(trFilename);
                                
                                Element trNSLink = doc.createElement("NSLink");
                                trNSLink.setTextContent(fileRelPath.substring(1));
                                newTranscription.appendChild(trNSLink);
                                
                                Element trDescription = doc.createElement("Description");
                                Element trKey = doc.createElement("Key");
                                trKey.setAttribute("Name", "segmented");
                                trKey.setTextContent("true");
                                trDescription.appendChild(trKey);
                                //leaving the rest empty, ComaSegmentCountChecker will fix it
                                newTranscription.appendChild(trDescription);
                                
                                Element trAvailability = doc.createElement("Availability");
                                Element trAvailable = doc.createElement("Available");
                                trAvailable.setTextContent("false");
                                trAvailability.appendChild(trAvailable);
                                Element trOI = doc.createElement("ObtainingInformation");
                                trAvailability.appendChild(trOI);
                                newTranscription.appendChild(trAvailability);
                                
                                ce.appendChild(newTranscription);
                                
                            } catch (XPathExpressionException ex) {
                                System.out.println("DEBUG MESSAGE: ComaFileCoverageChecker caught an exception while processing an XPath expression");
                            } catch (NullPointerException npe) {
                                //then a communication with this name does not exist in coma
                                String message = "File on filesystem is not explained in coma: " + fileRelPath;
                                ReportItem.createReportItem(true, this.getClass().getSimpleName(), filename, message);
                            }
                            
                        }
                        else if (fileRelPath.endsWith(".flextext")) {
                            String message = "File on filesystem is not explained in coma: " + fileRelPath;
                            ReportItem.createReportItem(false, this.getClass().getSimpleName(), filename, message);
                        } else {
                            String message = "File on filesystem is not explained in coma: " + fileRelPath;
                            ReportItem.createReportItem(true, this.getClass().getSimpleName(), filename, message);
                        }
                    }
                }
            }
        });
        
        return doc;
    }      
}
