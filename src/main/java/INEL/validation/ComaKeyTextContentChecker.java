package INEL.validation;

import INEL.ReportItem;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Properties;
import java.util.regex.Pattern;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

public class ComaKeyTextContentChecker {

    public ComaKeyTextContentChecker() {
    }
    
    public static ArrayList<String> availableFor() {
        ArrayList<String> fileTypes = new ArrayList<String> ();
        fileTypes.add("coma");
        return fileTypes;
    }
    
    public Document runChecker(Document doc, String filename, Boolean fix, Properties properties) throws SAXException, IOException, ParserConfigurationException, URISyntaxException {
        
        Pattern questionMarkPattern = Pattern.compile("^(\\s*\\?\\s*)+$");
        Pattern multipleWhitespacePattern = Pattern.compile("\\s{2,}");
        Pattern newlinePattern = Pattern.compile("^.*\\n.*$");
        
        ArrayList<String> keys2remove = new ArrayList<>();
        keys2remove.add("comment");
        keys2remove.add("project-name");
        keys2remove.add("transcription-convention");
        keys2remove.add("transcription-name");
        keys2remove.add("ud_flex2exb-settings");
        keys2remove.add("ud_flex2exb-timestamp");
        keys2remove.add("ud_flex2exb-version");
        
        NodeList keys = doc.getElementsByTagName("Key");
        
        for (int i = 0; i < keys.getLength(); i++) {
            Element key = (Element) keys.item(i);
            
            String keyText = key.getTextContent();
            
            if (keys2remove.contains(key.getAttribute("Name"))) {
                if (fix) {
                    key.getParentNode().removeChild(key);
                    i--;
                } else {
                    String message = "Key " + key.getAttribute("Name") + " needs to be removed";
                    ReportItem.createReportItem(true, this.getClass().getSimpleName(), filename, message);
                }
            }
            
            if (!keyText.isEmpty()) {
                if (questionMarkPattern.matcher(keyText).matches()) {
                    if (fix) {
                        key.setTextContent("...");
                    } else {
                        String message = "Element " + key.getAttribute("Name") + " has text value " + keyText;
                        ReportItem.createReportItem(false, this.getClass().getSimpleName(), filename, message);
                    }
                }
                if (multipleWhitespacePattern.matcher(keyText).find()) {
                    if (fix) {
                        key.setTextContent(keyText.replaceAll("\\s{2,}", "\\s"));
                    } else {
                        String message = "Element " + key.getAttribute("Name") + " contains multiple whitespaces: " + keyText;
                        ReportItem.createReportItem(false, this.getClass().getSimpleName(), filename, message);
                    }
                }
                if (newlinePattern.matcher(keyText).find()) {
                    if (fix) {
                        key.setTextContent(keyText.replaceAll("\\n", ""));
                    } else {
                        String message = "Element " + key.getAttribute("Name") + " contains newline: " + keyText;
                        ReportItem.createReportItem(true, this.getClass().getSimpleName(), filename, message);
                    }
                }
            } else {
                if (fix) {
                    key.setTextContent("...");
                } else {
                    String message = "Element " + key.getAttribute("Name") + " has an empty text value";
                        ReportItem.createReportItem(false, this.getClass().getSimpleName(), filename, message);
                }
            }
        }
                
        return doc;
    }      
}
