package INEL.validation;

import INEL.ReportItem;
import java.util.ArrayList;
import java.util.HashMap;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import javax.xml.parsers.ParserConfigurationException;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import java.util.Properties;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;
import org.apache.commons.cli.ParseException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class ComaLocationsChecker {

    public ComaLocationsChecker() {
    }

    public static ArrayList<String> availableFor() {
        ArrayList<String> fileTypes = new ArrayList<>();
        fileTypes.add("coma");
        return fileTypes;
    }

    public Document runChecker(Document doc, String filename, Boolean fix, Properties clprop) throws IOException, InterruptedException, ParserConfigurationException, ParseException, XPathExpressionException {
        //Curl to the geojson
        Process process = Runtime.getRuntime().exec("curl https://inel.corpora.uni-hamburg.de/portal/geo/doc.json");
        StringBuilder builder;
        try (BufferedReader reader = new BufferedReader(new InputStreamReader(process.getInputStream()))) {
            builder = new StringBuilder();
            String line;
            while ((line = reader.readLine()) != null) {
                builder.append(line);
            }
        }
        process.waitFor();
        String geoJsonString = builder.toString();
        //Parse the geojson
        JsonParser parser = new JsonParser();
        JsonObject jsonObject = (JsonObject) parser.parse(geoJsonString);
        JsonArray features = (JsonArray) jsonObject.get("features");
        HashMap<String, String[]> locationJson = new HashMap<>(); // hash map for holding coordinates of locations
        for (int l = 0; l < features.size(); l++) {
            JsonObject feature = (JsonObject) features.get(l);
            JsonObject geometry = (JsonObject) feature.get("geometry");
            JsonArray coordinates = (JsonArray) geometry.get("coordinates");
            JsonObject properties = (JsonObject) feature.get("properties");
            if (properties.has("name")) {
                String name = (String) properties.get("name").getAsString().trim();
                double featureLat = (double) coordinates.get(1).getAsDouble();
                double featureLon = (double) coordinates.get(0).getAsDouble();
                String[] coords = {Double.toString(featureLon), Double.toString(featureLat)};
                locationJson.put(name, coords);
            }
        }
        XPath xPath = XPathFactory.newInstance().newXPath();
        NodeList descriptions = (NodeList) xPath.compile("//Communication/Location/Description").evaluate(doc, XPathConstants.NODESET);
        for (int k = 0; k < descriptions.getLength(); k++) {
            Node descriptionNode = descriptions.item(k);

            NodeList keyNodes = (NodeList) xPath.compile("//Communication/Location/Description/Key").evaluate(descriptionNode, XPathConstants.NODESET);
            if (keyNodes.getLength() != 0) {
                // System.out.println(keyNodes.getLength());
                Node keyLngLat = (Node) xPath.compile("./Key[contains(@Name, 'Settlement (LngLat)')]").evaluate(descriptionNode, XPathConstants.NODE);
                //         System.out.println(keyLngLat);
                Element name = (Element) xPath.compile("./Key[contains(@Name, 'Settlement') and not(contains(@Name, '(LngLat)')) and not(contains(@Name, '(RU)'))]").evaluate(descriptionNode, XPathConstants.NODE);
                //   System.out.println(name);
                if (name != null) {
                    String nameLocationCommunication = name.getTextContent();
                    if (!nameLocationCommunication.equals("...") && keyLngLat != null) {
                        String keyLngLatValue = keyLngLat.getTextContent();
                        if (!keyLngLatValue.equals("...")) {
                            // Get the coordinates of the location
                            String[] coords = keyLngLatValue.split(",");
                            // Check if the name and coordinates match with the ones in the json hashmap
                            if (!locationJson.containsKey(nameLocationCommunication)) {
                                String message = nameLocationCommunication + " is not found in doc.json";
                                ReportItem.createReportItem(true, this.getClass().getSimpleName(), filename, message);
                                System.out.println(message);
                            } else {
                                String[] locationCoords = locationJson.get(nameLocationCommunication);
                                double lon = Double.parseDouble(locationCoords[0]);
                                double lat = Double.parseDouble(locationCoords[1]);
                                if (lon != Double.parseDouble(coords[0].trim()) || lat != Double.parseDouble(coords[1].trim())) {
                                    String message = nameLocationCommunication + " coordinates (lnglat) do not match with doc.json: " + lon + ", " + lat;
                                    ReportItem.createReportItem(true, this.getClass().getSimpleName(), filename, message);
                                    System.out.println(message);
                                }
                            }
                        } else {
                            if (locationJson.containsKey(nameLocationCommunication)) {
                                String[] locationCoords = locationJson.get(nameLocationCommunication);
                                double lon = Double.parseDouble(locationCoords[0]);
                                double lat = Double.parseDouble(locationCoords[1]);
                                //keyLngLat.setTextContent(lon + ", " + lat);
                                String message = nameLocationCommunication + " please add lnglat from doc.json: " + lon + ", " + lat;
                                ReportItem.createReportItem(true, this.getClass().getSimpleName(), filename, message);
                                System.out.println(message);
                            }
                        }
                    } //set one more check for name ... and existing lnglat? also maybe a check for russian names if the other two is empty?
                }
            }
        }
        NodeList speakers = doc.getElementsByTagName("Speaker");
        for (int p = 0; p < speakers.getLength(); p++) {
            Node speakersNode = speakers.item(p);
            NodeList speakerLocationNodes = ((Element) speakersNode).getElementsByTagName("Location");
            for (int j = 0; j < speakerLocationNodes.getLength(); j++) {
                Node locationNode = speakerLocationNodes.item(j);
                Node typeAttribute = locationNode.getAttributes().getNamedItem("Type");
                if (typeAttribute != null && (typeAttribute.getTextContent().equals("Basic biogr. data") || typeAttribute.getTextContent().equals("Basic biographic data"))) {
                    NodeList keys = ((Element) locationNode).getElementsByTagName("Key");
                    String nameLocationDomicile = null;
                    String nameLocationBirthPlace = null;
                    String[] coordsDomicile = null;
                    String[] coordsBirthPlace = null;
                    for (int k = 0; k < keys.getLength(); k++) {
                        Node keyNode = keys.item(k);
                        Node nameAttribute = keyNode.getAttributes().getNamedItem("Name");
                        if (nameAttribute != null) {
                            String keyName = nameAttribute.getTextContent();
                            if (keyName.contains("7a Domicile") || keyName.contains("7a Current residence") || keyName.contains("7 Domicile")) {
                                nameLocationDomicile = keyNode.getTextContent();
                            }
                            if (keyName.contains("7c Domicile (LngLat)") || keyName.contains("7 Domicile (LngLat)") || keyName.contains("7c Current residence (LngLat)")) {
                                coordsDomicile = keyNode.getTextContent().split(",");
                            }
                            if (keyName.contains("1a Place of birth") || keyName.contains("1 Place of birth")) {
                                nameLocationBirthPlace = keyNode.getTextContent();
                            }
                            if (keyName.contains("1a Place of birth (LngLat)") || keyName.contains("1 Place of birth (LngLat)")) {
                                coordsBirthPlace = keyNode.getTextContent().split(",");
                            }
                        }
                    }
                    if (!"...".equals(nameLocationDomicile)) {
                        if (coordsDomicile != null) {
                            if (!locationJson.containsKey(nameLocationDomicile)) {
                                String message = "Domicile " + nameLocationDomicile + ": not found in doc.json";
                                ReportItem.createReportItem(false, this.getClass().getSimpleName(), filename, message);
                                System.out.println(message);
                            } else {
                                String[] jsonCoords = locationJson.get(nameLocationDomicile);
                                if (jsonCoords != null && coordsDomicile.length >= 2) {
                                    double lon = Double.parseDouble(jsonCoords[0]);
                                    double lat = Double.parseDouble(jsonCoords[1]);
                                    if (!jsonCoords[0].equals(coordsDomicile[0].trim()) || !jsonCoords[1].equals(coordsDomicile[1].trim())) {
                                        String message = "Domicile " + nameLocationDomicile + ": coordinates do not match with doc.json";
                                        ReportItem.createReportItem(false, this.getClass().getSimpleName(), filename, message);
                                        System.out.println(message);
                                    }
                                }
                            }
                        } else {
                            if (!locationJson.containsKey(nameLocationDomicile)) {
                                String message = "Domicile " + nameLocationDomicile + ": not found in doc.json";
                                ReportItem.createReportItem(false, this.getClass().getSimpleName(), filename, message);
                                System.out.println(message);

                            }
                        }
                        if (!"...".equals(nameLocationBirthPlace)) {
                            if (coordsBirthPlace == null || coordsBirthPlace.length < 2) {
                                String message = "Birthplace " + nameLocationBirthPlace + ": coordinates not found in comafile";
                                ReportItem.createReportItem(false, this.getClass().getSimpleName(), filename, message);
                                System.out.println(message);
                            } else {
                                if (!locationJson.containsKey(nameLocationBirthPlace)) {
                                    String message = "Birthplace " + nameLocationBirthPlace + ": not found in doc.json";
                                    ReportItem.createReportItem(false, this.getClass().getSimpleName(), filename, message);
                                    System.out.println(message);
                                } else {
                                    String[] jsonCoords = locationJson.get(nameLocationBirthPlace);
                                    if (jsonCoords != null && coordsBirthPlace.length >= 2) {
                                        double lon = Double.parseDouble(jsonCoords[0]);
                                        double lat = Double.parseDouble(jsonCoords[1]);
                                        if (!jsonCoords[0].equals(coordsBirthPlace[0].trim()) || !jsonCoords[1].equals(coordsBirthPlace[1].trim())) {
                                            String message = "Birthplace " + nameLocationBirthPlace + ": coordinates do not match with doc.json";
                                            ReportItem.createReportItem(false, this.getClass().getSimpleName(), filename, message);
                                            System.out.println(message);
                                        }
                                    }
                                }

                            }
                        }
                    }
                }
            }

        }
        return doc;
    }
}
// }return doc; **/ 
/*   NodeList speakers = doc.getElementsByTagName("Speaker");
        for (int p = 0; p < speakers.getLength(); p++) {
            Node speakersNode = speakers.item(p);
            NodeList speakerLocationNodes = ((Element) speakersNode).getElementsByTagName("Location");
            for (int j = 0; j < speakerLocationNodes.getLength(); j++) {
                Node locationNode = speakerLocationNodes.item(j);
                if (locationNode.getAttributes().getNamedItem("Type").getTextContent().equals("Basic biogr. data")) {
                    NodeList keys = ((Element) locationNode).getElementsByTagName("Key");
                    String nameLocationDomicile = null;
                    String[] coords = null;
                    for (int k = 0; k < keys.getLength(); k++) {
                        Node keyNode = keys.item(k);
                        String keyName = keyNode.getAttributes().getNamedItem("Name").getTextContent();
                        if (keyName.equals("7a Domicile")) {
                            nameLocationDomicile = keyNode.getTextContent();
                        } else if (keyName.equals("7c Domicile (LngLat)")) {
                            coords = keyNode.getTextContent().split(",");
                        }
                    }
                    if (nameLocationDomicile != null && coords != null && locationJson.containsKey(nameLocationDomicile) && locationJson.get(nameLocationDomicile)[0].equals(coords[0].trim()) && locationJson.get(nameLocationDomicile)[1].equals(coords[1].trim())) {
                        //System.out.println(nameDomicile + " match domicile");
                    }
                }
            }
        }*/
