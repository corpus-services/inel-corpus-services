package INEL.validation;

import INEL.utilities.XSLTransformer;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Properties;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;

public class ComaOverviewGeneration {

    public ComaOverviewGeneration() {
    }
    
    public static ArrayList<String> availableFor() {
        ArrayList<String> fileTypes = new ArrayList<String> ();
        fileTypes.add("coma");
        return fileTypes;
    }
    
    public Document runChecker(Document doc, String filename, Boolean fix, Properties properties) throws SAXException, IOException, ParserConfigurationException, URISyntaxException {
        
        //set to true by default for INEL use
        boolean inel = true; 
        String xslpath = "/xsl/Output_metadata_summary.xsl";
    
        String xsl;
        try {
            URL docURL = new URL(doc.getBaseURI());
            File coma = new File(docURL.getFile());
            String xml = FileUtils.readFileToString(coma, "utf-8");

            // get the XSLT stylesheet as String            
            java.util.Scanner s = new java.util.Scanner(getClass().getResourceAsStream(xslpath)).useDelimiter("\\A");
            xsl = s.hasNext() ? s.next() : "";
            
            // create XSLTransformer and set the parameters 
            XSLTransformer xt = new XSLTransformer();
            //set an parameter for INEL
            if(inel){  
                xt.setParameter("mode", "inel");
            }
            // perform XSLT transformation
            
            String result = xt.transform(xml, xsl);
            //get location to save new result
            String outfile = FilenameUtils.getFullPath(doc.getBaseURI()) + "curation/coma_overview.html";
            URL overviewurl = new URL(outfile);
            File outputfile = new File(overviewurl.getFile());
            
            try (Writer writer = new OutputStreamWriter(new FileOutputStream(outputfile), StandardCharsets.UTF_8)) {
                writer.write(result);
            }

        } catch (TransformerConfigurationException ex) {
            System.out.println("Transformer configuration error");
        } catch (TransformerException ex) {
            System.out.println("Transformer error");
        } catch (MalformedURLException ex) {
            System.out.println("Malformed URL error");
        } catch (IOException ex) {
            System.out.println("Unknown input/output error");
        } catch (Exception e) {
            System.out.println("Caught " + e);
        } 
                
        return doc;
    }      
}
