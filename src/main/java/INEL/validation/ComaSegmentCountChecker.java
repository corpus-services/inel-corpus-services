package INEL.validation;

import INEL.CorpusFunctions;
import INEL.ReportItem;
import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Properties;
import javax.xml.parsers.ParserConfigurationException;
import org.jdom2.JDOMException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

public class ComaSegmentCountChecker {

    public ComaSegmentCountChecker() {
    }
    
    public static ArrayList<String> availableFor() {
        ArrayList<String> fileTypes = new ArrayList<String> ();
        fileTypes.add("coma");
        return fileTypes;
    }
    
    public Document runChecker(Document doc, String filename, Boolean fix, Properties properties) throws SAXException, IOException, ParserConfigurationException, URISyntaxException, JDOMException {
        
        NodeList transcriptions = doc.getElementsByTagName("Transcription"); // divide by Communication tags
        ArrayList<String> algorithmNames = new ArrayList<>(); // array for holding algorithm names
            
        for (int i = 0; i < transcriptions.getLength(); i++) {

            Element transcription = (Element) transcriptions.item(i);
            NodeList NSLinks = transcription.getElementsByTagName("NSLink");
            if (NSLinks.getLength() == 1) {
                String NSLink = NSLinks.item(0).getTextContent();
                if (NSLink.endsWith("_s.exs")) {
                    URL url = new URL(doc.getDocumentURI().substring(0, doc.getDocumentURI().lastIndexOf("/") + 1) + NSLink);
                    File exsfile = new File(url.getFile());
                    Document exs = CorpusFunctions.buildXMLDocument(exsfile);
                    HashMap<String,String> segmentCounts = this.getSegmentCounts(exs);

                    NodeList keys = transcription.getElementsByTagName("Key");
                    for (int k = 0; k < keys.getLength(); k++) {
                        Element key = (Element) keys.item(k);
                        String keyname = key.getAttribute("Name");
                        if (keyname.startsWith("#")) {
                            String value = segmentCounts.get(keyname);
                            if (fix) {
                                if (!value.equals(key.getTextContent())) {
                                    key.setTextContent(value);
                                }
                            }
                            if (keyname.contains(":")) {
                                try {
                                    String algName = keyname.substring(2, keyname.lastIndexOf(":"));
                                    if (!algorithmNames.contains(algName)) {
                                        algorithmNames.add(algName);
                                    }
                                } catch (Exception e) {
                                    System.out.println("DEBUG MESSAGE: ComaSegmentCountChecker caught exception " + e);
                                }
                            }
                            segmentCounts.remove(keyname);
                        }
                    }
                    if (fix) {
                        if (!segmentCounts.isEmpty()) {
                            Element description = (Element) transcription.getElementsByTagName("Description").item(0);
                            segmentCounts.forEach((k,v) -> {
                                Element newkey = doc.createElement("Key");
                                newkey.setAttribute("Name", k);
                                newkey.setTextContent(v);
                                description.appendChild(newkey);
                            });
                        } 
                    }
                }
            }
        }
        if (algorithmNames.size() > 1) {
            String message = "Coma file contains different segmentation algorithms: " + algorithmNames;
            ReportItem.createReportItem(true, this.getClass().getSimpleName(), filename, message);
        } else if (algorithmNames.isEmpty()) {
            String message = "No segment counts added yet. Use Coma > Maintenance > Update segment counts to add them. ";
            ReportItem.createReportItem(false, this.getClass().getSimpleName(), filename, message);
        }
        
        return doc;
    }
    
    private HashMap<String, String> getSegmentCounts(Document exs) throws JDOMException {
        HashMap<String, String> segmentCounts = new HashMap<>();
        NodeList udInfo = exs.getElementsByTagName("ud-information");
        for (int s = 0; s < udInfo.getLength(); s++) {
            Element se = (Element) udInfo.item(s);
            String name = se.getAttribute("attribute-name");
            if (name.startsWith("#")) {
                String value = se.getTextContent();
                segmentCounts.put(name, value);
            }
        }
        
        return segmentCounts;
    }
}
