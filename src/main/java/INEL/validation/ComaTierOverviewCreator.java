/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package INEL.validation;

import static INEL.CorpusFunctions.buildXMLDocument;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.List;
import java.util.Properties;
import java.util.Set;
import java.util.TreeSet;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathFactory;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.jdom.JDOMException;
import org.jdom.input.SAXBuilder;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

/**
 *
 * @author uranus
 */
public class ComaTierOverviewCreator {
    
    public ComaTierOverviewCreator() {
    }
    
    public static ArrayList<String> availableFor() {
        ArrayList<String> fileTypes = new ArrayList<String> ();
        fileTypes.add("coma");
        return fileTypes;
    }
    
    public Document runChecker(Document doc, String filename, Boolean fix, Properties properties) throws SAXException, IOException, ParserConfigurationException, URISyntaxException, JDOMException {
        
        String xslpath = "/xsl/tier_overview_datatable_template.html";
        
        String htmltemplate;
        java.util.Scanner scan = new java.util.Scanner(getClass().getResourceAsStream(xslpath)).useDelimiter("\\A");
        htmltemplate = scan.hasNext() ? scan.next() : "";

        StringBuilder overviewTable = new StringBuilder();
        StringBuilder communicationsTable = new StringBuilder();

        SAXBuilder builder = new SAXBuilder();
        URL docURL = new URL(doc.getBaseURI());
        String corpusBaseDir = doc.getBaseURI().substring(0, doc.getBaseURI().lastIndexOf("/") + 1);
        org.jdom.Document readcomaasjdom = builder.build(docURL);
        Collection<URL> resulturls = new ArrayList<>();

        try {
            javax.xml.xpath.XPath xp = XPathFactory.newInstance().newXPath();
            String basicFileXPath = "//Transcription[Description/Key[@Name='segmented']/text()='false']/NSLink";
            NodeList transcriptionList = (NodeList) xp.compile(basicFileXPath).evaluate(doc, XPathConstants.NODESET);

            for (int n = 0; n < transcriptionList.getLength(); n++) {
                String nslink = (transcriptionList.item(n).getTextContent());
                URL resulturl = new URL(corpusBaseDir + nslink);
                resulturls.add(resulturl);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        List<String> stringtiers = new ArrayList<>();
        List<String> fileTiers = new ArrayList<>();
        List<String> nonEmptyTiers = new ArrayList<>();
        HashMap<String, HashMap> fileStats = new HashMap();

        for (URL resulturl : resulturls) {
            File f = new File(resulturl.getFile()); 
            if (f.exists() && f.isFile()) {
                HashMap<String, HashMap> speakerStats = new HashMap();
                Document exb = buildXMLDocument(f);
                NodeList ts = exb.getElementsByTagName("tier");
                List<String> tiersInFile = new ArrayList<>();
                for (int tf = 0; tf < ts.getLength(); tf++) {
                    HashMap<String, Integer> tierStats = new HashMap();
                    Element tier = (Element) ts.item(tf);
                    String category = tier.getAttribute("category");
                    String speaker = tier.getAttribute("speaker");
                    stringtiers.add(category + " (type: " + tier.getAttribute("type") + ")");
                    if (!tiersInFile.contains(category)) {
                        tiersInFile.add(category);
                        fileTiers.add(category + " (type: " + tier.getAttribute("type") + ")");
                    }

                    int numEvents = tier.getElementsByTagName("event").getLength();
                    if (numEvents > 0) {
                        nonEmptyTiers.add(category + " (type: " + tier.getAttribute("type") + ")");
                    }
                    tierStats.put(category, numEvents);
                    if (speakerStats.containsKey(speaker)) {
                        speakerStats.get(speaker).put(category, numEvents);
                    } else {
                        speakerStats.put(speaker, tierStats);
                    }
                }
                fileStats.put(f.getName(), speakerStats);
            } 
        }

        Set<String> hash_Set = new TreeSet<>(stringtiers);
        //now we have all the existing tiers from the exbs, we need to make a table out of it
        //use the html template and add the content into id
        if (!stringtiers.isEmpty()) {
            // get the HTML stylesheet
            overviewTable.append("<h1> Tier Overview over Whole Corpus (");
            overviewTable.append(resulturls.size());
            overviewTable.append(" exbs) </h1>");
            overviewTable.append("<table id=\"\" class=\"compact\">\n"
                    + "   <thead>\n"
                    + "      <tr>\n"
                    + "         <th class=\"compact\">Category-Type-DisplayName</th>\n"
                    + "         <th class=\"compact\">Number of Files with Tier</th>\n"
                    + "         <th class=\"compact\">Number of Tiers</th>\n"
                    + "         <th class=\"compact\">Number of Non-Empty Tiers</th>\n"
                    + "      </tr>\n"
                    + "   </thead>\n"
                    + "   <tbody>\n");
            // add the tables to the html
            //first table: one column with categories, one with count
            // add the overviewTable to the html
            //first table: one column with categories, one with count

            for (String s : hash_Set) {
                overviewTable.append("<tr><td class=\"compact\">");
                overviewTable.append(s);
                overviewTable.append("</td><td class=\"compact\">");
                overviewTable.append(Collections.frequency(fileTiers, s));
                overviewTable.append("</td><td class=\"compact\">");
                overviewTable.append(Collections.frequency(stringtiers, s));
                overviewTable.append("</td><td class=\"compact\">");
                overviewTable.append(Collections.frequency(nonEmptyTiers, s));
                overviewTable.append("</td></tr>");
            }
            overviewTable.append("   </tbody>\n"
                    + "</table>");
        } else {
            System.out.println("DEBUG MESSAGE: No tiers found in linked exbs");
        }
        //now each exb linked in the coma file
        if (!fileStats.isEmpty()) {
            communicationsTable.append("<h1> Tiers in each exb </h1>");
            //first is the column for filename, then all the tier category/type combinations
            communicationsTable.append("<table id=\"\" class=\"compact\">\n"
                    + "   <thead>\n"
                    + "<th class=\"compact\"> Exb Filename </th>"
                    + "<th class=\"compact\"> Speaker </th>");
            for (String s : hash_Set) {
                communicationsTable.append("<th class=\"compact\">");
                communicationsTable.append(s);
                communicationsTable.append("</th>");
            }
            communicationsTable.append("</tr>"
                    + "   </thead>\n"
                    + "   <tbody>\n");

            for (Map.Entry<String, HashMap> fileEntry : fileStats.entrySet()) {
                HashMap<String, HashMap> fileSt = fileEntry.getValue();
                //first is the column for filename, then all the tier category/type combinations
                for (Map.Entry<String, HashMap> speakerEntry : fileSt.entrySet()) {
                    //filename
                    communicationsTable.append("<tr><td class=\"compact\">");
                    communicationsTable.append(fileEntry.getKey());
                    communicationsTable.append("</td>");
                    
                    //speaker code
                    communicationsTable.append("<td class=\"compact\">");
                    communicationsTable.append(speakerEntry.getKey());
                    communicationsTable.append("</td>");
                    
                    HashMap<String, Integer> speakerSt = speakerEntry.getValue();
                    for (String s : hash_Set) {
                        String[] catType = s.split("type: ");
                        String category = catType[0].substring(0, catType[0].length() - 2);
                        //String type = catType[1].substring(0, catType[1].length() - 1);

                        if (speakerSt.containsKey(category)) {
                            communicationsTable.append("<td class=\"compact\">");
                            communicationsTable.append(speakerSt.get(category).toString());
                            communicationsTable.append("</td>");
                        } else {
                            communicationsTable.append("<td class=\"compact\">no</td>");
                        }
                    }
                }             
                communicationsTable.append("</tr>");
            }

            communicationsTable.append(" </tr>\n"
                    + "   </tbody>\n"
                    + "</table>");
        } else {
            System.out.println("DEBUG MESSAGE: No tiers found in linked exbs");
        }

        StringBuilder htmlend = new StringBuilder();
        htmlend.append("   </body>\n</html>");

        //add timestamp
        StringBuilder timestamp = new StringBuilder();     
        timestamp.append("   <div id='timestamp'>Generated: ");        
        Timestamp time = new Timestamp(System.currentTimeMillis());
        timestamp.append(time);
        timestamp.append("</div>\n");

        StringBuilder resultString = new StringBuilder();
        resultString.append(htmltemplate);
        resultString.append(timestamp);
        resultString.append(overviewTable.toString());
        resultString.append(communicationsTable.toString());
        resultString.append(htmlend);

        String result = resultString.toString();

        String outfile = FilenameUtils.getFullPath(doc.getBaseURI()) + "curation/tier_overview.html";
        URL overviewurl = new URL(outfile);
        File outputfile = new File(overviewurl.getFile());

        try (Writer writer = new OutputStreamWriter(new FileOutputStream(outputfile), StandardCharsets.UTF_8)) {
            writer.write(result);
        }

        return doc; 
    }
}
