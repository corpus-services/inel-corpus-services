package INEL.validation;

import INEL.ReportItem;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Properties;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

public class ComaTranscriptionsNameChecker {

    public ComaTranscriptionsNameChecker() {
    }
    
    public static ArrayList<String> availableFor() {
        ArrayList<String> fileTypes = new ArrayList<String> ();
        fileTypes.add("coma");
        return fileTypes;
    }
    
    public Document runChecker(Document doc, String filename, Boolean fix, Properties properties) throws SAXException, IOException, ParserConfigurationException {
        
        String baseURI = doc.getBaseURI();
        String basePath = "";
        if (baseURI.contains("/")) { //Linux path
            basePath = doc.getBaseURI().substring(0, doc.getBaseURI().lastIndexOf("/") + 1);
        } else if (baseURI.contains("\\")) { //Win path
            basePath = doc.getBaseURI().substring(0, doc.getBaseURI().lastIndexOf("\\") + 1);
        } 
        NodeList communications = doc.getElementsByTagName("Communication"); // divide by Communication tags
        for (int i = 0; i < communications.getLength(); i++) { //iterate through communications
            Element communication = (Element) communications.item(i);
            NodeList transcriptions = communication.getElementsByTagName("Transcription"); // get transcriptions of current communication
            String communicationID = communication.getAttribute("Id"); // get communication id to use it in the warning
            String communicationName = communication.getAttribute("Name"); // get communication name to use it in the warning
            
            String basicTranscriptName = "";
            String basicFileName = "";
            String basicNSLink = "";
            String segmentedTranscriptName = "";
            String segmentedFileName = "";
            String segmentedNSLink = "";

            if (transcriptions.getLength() > 0) {  // check if there is at least one transcription for the communication
                for (int j = 0; j < transcriptions.getLength(); j++) {   // iterate through transcriptions
                    Element transcription = (Element) transcriptions.item(j);
                                       
                    if (transcription.getElementsByTagName("Filename").item(0).getTextContent().endsWith(".exb")) { //then it's a basic transcription
                        basicTranscriptName = transcription.getElementsByTagName("Name").item(0).getTextContent();
                        basicFileName = transcription.getElementsByTagName("Filename").item(0).getTextContent();
                        basicNSLink = transcription.getElementsByTagName("NSLink").item(0).getTextContent();
                    } else if (transcription.getElementsByTagName("Filename").item(0).getTextContent().endsWith(".exs")) { //segmented transcription
                        segmentedTranscriptName = transcription.getElementsByTagName("Name").item(0).getTextContent();
                        segmentedFileName = transcription.getElementsByTagName("Filename").item(0).getTextContent();
                        segmentedNSLink = transcription.getElementsByTagName("NSLink").item(0).getTextContent();
                    } else { //something weird is present - raise an error
                        String message = communicationName + ", id: " + communicationID + " has a transcription that is neither basic nor segmented.";
                        ReportItem.createReportItem(true, this.getClass().getSimpleName(), filename, message);
                    }
                }
                
                //comparing the names 
                    
                if (!basicTranscriptName.isEmpty() && !segmentedTranscriptName.isEmpty()) {
                    // check for mismatch between transcription names
                    if (!basicTranscriptName.equals(segmentedTranscriptName)) { 
                        String message = "Basic transcription name and segmented transcription name do not match " 
                                + "for basic transcription name " + basicTranscriptName + ", transcription name: " + segmentedTranscriptName + ".";
                        ReportItem.createReportItem(true, this.getClass().getSimpleName(), filename, message);
                    }
                    if (!basicTranscriptName.equals(communicationName)) { 
                        String message = "Basic transcription name and communication name do not match " 
                                + "for communication " + communicationName + ", transcription name: " + basicTranscriptName + ".";
                        ReportItem.createReportItem(true, this.getClass().getSimpleName(), filename, message);
                    }
                    if (!segmentedTranscriptName.equals(communicationName)) { 
                        String message = "Segmented transcription name and communication name do not match " 
                                + "for communication " + communicationName + ", transcription name: " + segmentedTranscriptName + ".";
                        ReportItem.createReportItem(true, this.getClass().getSimpleName(), filename, message);
                    }
                }                
                
                if (!basicFileName.isEmpty() && !segmentedFileName.isEmpty()) {
                    // check for mismatch between file names, issue a warning if necessary
                    if (!basicFileName.substring(0, basicFileName.lastIndexOf(".")).equals(segmentedFileName.substring(0, segmentedFileName.lastIndexOf("_")))) {
                        String message = "Basic file name and segmented file name do not match "
                                + "for filename " + basicFileName + ", filename: " + segmentedFileName + ".";
                        ReportItem.createReportItem(true, this.getClass().getSimpleName(), filename, message);
                    } 
                    if (!basicFileName.substring(0, basicFileName.lastIndexOf(".")).equals(communicationName)) {
                        String message = "Basic file name and communication name do not match "
                                + "for communication " + communicationName + ", filename: " + basicFileName + ".";
                        ReportItem.createReportItem(true, this.getClass().getSimpleName(), filename, message);
                    }
                    if (!communicationName.equals(segmentedFileName.substring(0, segmentedFileName.lastIndexOf("_")))) {
                        String message = "Communication name and segmented file name do not match "
                                + "for communication " + communicationName + ", filename: " + segmentedFileName + ".";
                        ReportItem.createReportItem(true, this.getClass().getSimpleName(), filename, message);
                    }
                }
                
                if (!basicNSLink.isEmpty() && !segmentedNSLink.isEmpty()) {   
                    //check if the files exist
                    URL basicURL = new URL(basePath + basicNSLink);
                    File basicNS = new File(basicURL.getFile());
                    if (!basicNS.exists()) {
                        String message = "File in NSLink " + basicNSLink + "for communication "
                                + communicationName + ", id: " + communicationID + " not found.";
                        ReportItem.createReportItem(true, this.getClass().getSimpleName(), filename, message);
                    }
                    
                    URL segmentedURL = new URL(basePath + segmentedNSLink);
                    File segmentedNS = new File(segmentedURL.getFile());
                    if (!segmentedNS.exists()) {
                        String message = "File in NSLink " + segmentedNSLink + "for communication "
                                + communicationName + ", id: " + communicationID + " not found.";
                        ReportItem.createReportItem(true, this.getClass().getSimpleName(), filename, message);
                    }
                    
                    // check for mismatch between nslinks, issue a warning if necessary
                    if (!basicNSLink.substring(0, basicNSLink.lastIndexOf(".")).equals(basicNSLink.substring(0, segmentedNSLink.lastIndexOf("_")))) {
                        String message = "Basic NSLink and segmented NSLink do not match "
                                + "for segmented NSLink " + segmentedNSLink + ", basic NSLink: " + basicNSLink + ".";
                        ReportItem.createReportItem(true, this.getClass().getSimpleName(), filename, message);
                    }
                    
                    if (!basicFileName.isEmpty() && !segmentedFileName.isEmpty()) {
                        if (!basicNSLink.substring(0, basicNSLink.lastIndexOf(".")).endsWith(basicFileName.substring(0, basicFileName.lastIndexOf(".")))) {
                            String message = "Basic NSLink and basic filename do not match "
                                    + "for filename " + basicFileName + ", NSLink: " + basicNSLink + ".";
                            ReportItem.createReportItem(true, this.getClass().getSimpleName(), filename, message);
                        }
                        if (!segmentedNSLink.substring(0, segmentedNSLink.lastIndexOf("_")).endsWith(segmentedFileName.substring(0, segmentedFileName.lastIndexOf("_")))) {
                            String message = "Segmented NSLink and segmented filename do not match "
                                    + "for filename " + segmentedFileName + ", NSLink: " + segmentedNSLink + ".";
                            ReportItem.createReportItem(true, this.getClass().getSimpleName(), filename, message);
                        }
                    }
                }
            }
            
            NodeList recordings = communication.getElementsByTagName("Recording");
            
            String recordingName = "";
            String recordingFilename = "";
            String recordingNSLink = "";
            
            if (recordings.getLength() > 0) {  // check if there is at least one recording for the communication
                for (int j = 0; j < recordings.getLength(); j++) {   // iterate through recordings
                    Element recording = (Element) recordings.item(j);
                    
                    recordingName = recording.getElementsByTagName("Name").item(0).getTextContent();
                    
                    NodeList media = recording.getElementsByTagName("Media");
                    if (media.getLength() > 0) {
                        
                        for (int k = 0; k < media.getLength(); k++) {
                            
                            Element mediaEl = (Element) media.item(k);

                            if (mediaEl.getElementsByTagName("Filename").getLength() > 0) {
                                recordingFilename = mediaEl.getElementsByTagName("Filename").item(0).getTextContent();
                                if (!recordingFilename.substring(0, recordingFilename.lastIndexOf(".")).equals(communicationName)) {
                                    String message = "Recording file name and communication name do not match "
                                            + "for communication " + communicationName + ", recording: " + recordingFilename + ".";
                                    ReportItem.createReportItem(true, this.getClass().getSimpleName(), filename, message);
                                }
                            } else {
                                if (fix) {
                                    try {
                                        Element recFilename = doc.createElement("Filename");
                                        String fnameExtension = mediaEl.getElementsByTagName("NSLink").item(0).getTextContent();
                                        fnameExtension = fnameExtension.substring(fnameExtension.lastIndexOf("."));
                                        recFilename.setTextContent(communicationName + fnameExtension);
                                        System.out.println(recFilename.getTextContent());
                                        mediaEl.appendChild(recFilename);
                                    } catch (Exception e) {
                                        String message = "No recording filename for communication " + communicationName + ", id: " + communicationID + ".";
                                        ReportItem.createReportItem(true, this.getClass().getSimpleName(), filename, message);
                                    }
                                } else {
                                    String message = "No recording filename for communication " + communicationName + ", id: " + communicationID + ".";
                                    ReportItem.createReportItem(true, this.getClass().getSimpleName(), filename, message);
                                }
                            }

                            if (mediaEl.getElementsByTagName("NSLink").getLength() > 0) {
                                recordingNSLink = mediaEl.getElementsByTagName("NSLink").item(0).getTextContent();
                                
                                //check if files exist
                                URL recordingURL = new URL(basePath + recordingNSLink);
                                File recordingNS = new File(recordingURL.getFile());
                                if (!recordingNS.exists()) {
                                    String message = "Recording in NSLink " + recordingNS + "for communication "
                                            + communicationName + ", id: " + communicationID + " not found.";
                                    ReportItem.createReportItem(true, this.getClass().getSimpleName(), filename, message);
                                }

                                if (!recordingNSLink.substring(0, recordingNSLink.lastIndexOf(".")).endsWith(communicationName)) {
                                    String message = "Recording NSLink and communication name do not match "
                                            + "for communication " + communicationName + ", NSLink: " + recordingNSLink + ".";
                                    ReportItem.createReportItem(true, this.getClass().getSimpleName(), filename, message);
                                }
                            } else {
                                String message = "No recording NSLink for communication" + communicationName + ", id: " + communicationID + ".";
                                ReportItem.createReportItem(true, this.getClass().getSimpleName(), filename, message);
                            }
                        }
                        
                    } else {
                        String message = communicationName + ", id: " + communicationID + " has a recording entry " + recordingName + " without any mediafile linked to it";
                        ReportItem.createReportItem(true, this.getClass().getSimpleName(), filename, message);
                    }
                    
                    if (!recordingName.equals(communicationName)) {
                        String message = "Recording name and communication name do not match "
                                + "for communication " + communicationName + ", recording: " + recordingName + ".";
                        ReportItem.createReportItem(true, this.getClass().getSimpleName(), filename, message);
                    } 
                }
            }
        }
        
        return doc;
    }      
}
