package INEL.validation;

import INEL.ReportItem;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Properties;
import java.util.regex.Pattern;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

public class ExbEventTextContentChecker {

    public ExbEventTextContentChecker() {
    }
    
    public static ArrayList<String> availableFor() {
        ArrayList<String> fileTypes = new ArrayList<String> ();
        fileTypes.add("exb");
        return fileTypes;
    }
    
    public Document runChecker(Document doc, String filename, Boolean fix, Properties properties) throws SAXException, IOException, ParserConfigurationException {
        
        Pattern linebreakPattern = Pattern.compile("(\\r\\n|\\r|\\n|\\s{2,})");
        Pattern questionMarkPattern = Pattern.compile("^(\\s*\\?\\s*)+$");
        Pattern attachesToAnyPattern = Pattern.compile("Attaches.*?to.*?any.*?category");
        Pattern tripleDotPattern = Pattern.compile("(?<!\\.)\\.{3}(?!\\.)");
        Pattern multiDotPattern = Pattern.compile("(?<!\\.)(\\.{2}|\\.{4,})(?!\\.)");
        String s = "";
        NodeList allContextInstances = doc.getElementsByTagName("event");
        for (int i = 0; i < allContextInstances.getLength(); i++) {
            Element e = (Element) allContextInstances.item(i);
            s = e.getTextContent();
            if (s.isBlank()) {
                if (fix) {
                    e.getParentNode().removeChild(e);
                    System.out.println("Empty event was removed");
                } else {
                    String message = "Empty events need to be removed";
                    ReportItem.createReportItem(true, this.getClass().getSimpleName(), filename, message);
                }
            } else {
                if (s.contains("§")) {
                    String message = "Exb has a § sign in the event " + s;
                    ReportItem.createReportItem(true, this.getClass().getSimpleName(), filename, message);
                }
                if (attachesToAnyPattern.matcher(s).find()) {
                    String message = "Exb has an event with Attaches to any category: " + s;
                    ReportItem.createReportItem(false, this.getClass().getSimpleName(), filename, message);
                }
                if (linebreakPattern.matcher(s).find()) {
                    if (fix) {
                        e.setTextContent(s.replaceAll("(\\r\\n|\\r|\\n)", ""));
                        e.setTextContent(s.replaceAll("\\s{2,}", " "));
                    } else {
                        String message = "Exb is containing line ending or several whitespace characters in an event: " + s;
                        ReportItem.createReportItem(true, this.getClass().getSimpleName(), filename, message);
                    }
                } 
                if (questionMarkPattern.matcher(s).matches()) {
                    String message = "Exb has an event containing only question marks: " + s;
                    ReportItem.createReportItem(false, this.getClass().getSimpleName(), filename, message);
                }
                if (tripleDotPattern.matcher(s).find()) {
                    if (fix) {
                        e.setTextContent(s.replaceAll("(?<!\\.)\\.{3}(?!\\.)", "…"));
                    } else {
                        String message = "Exb has an event with an ellipsis candidate: " + s;
                        ReportItem.createReportItem(false, this.getClass().getSimpleName(), filename, message);
                    }
                }
                if (multiDotPattern.matcher(s).find()) {
                    Pattern fourDotPattern = Pattern.compile("(?<!\\.)\\.{4,}(?!\\.)");
                    if (fix && fourDotPattern.matcher(s).find()) {
                        e.setTextContent(s.replaceAll("(?<!\\.)\\.{4,}(?!\\.)", "…"));
                    } else {
                        Element tier = (Element) e.getParentNode();
                        if (tier.getAttribute("category").equals("ts") || tier.getAttribute("category").equals("fe")
                                || tier.getAttribute("category").equals("fg") || tier.getAttribute("category").equals("fr")) {
                            String message = "Exb has an event with a wrong number of dots: " + s + " at tier " 
                                + tier.getAttribute("display-name") + " starting at " + e.getAttribute("start");
                            ReportItem.createReportItem(true, this.getClass().getSimpleName(), filename, message);
                        } else {
                            String message = "Exb has an event with a wrong number of dots: " + s + " at tier " 
                                    + tier.getAttribute("display-name") + " starting at " + e.getAttribute("start");
                            ReportItem.createReportItem(false, this.getClass().getSimpleName(), filename, message);
                        }
                    }
                }
            }
        }
        
        return doc;
    }      
}
