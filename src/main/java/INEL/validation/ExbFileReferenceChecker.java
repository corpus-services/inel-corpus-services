package INEL.validation;

import INEL.ReportItem;
import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Properties;
import java.util.regex.Pattern;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

public class ExbFileReferenceChecker {

    public ExbFileReferenceChecker() {
    }
    
    public static ArrayList<String> availableFor() {
        ArrayList<String> fileTypes = new ArrayList<String> ();
        fileTypes.add("exb");
        fileTypes.add("exs");
        return fileTypes;
    }
    
    public Document runChecker(Document doc, String filename, Boolean fix, Properties properties) throws SAXException, IOException, ParserConfigurationException, URISyntaxException {
        
        //only turning this on for EXBs, as EXSs are created automatically anyways
        if (filename.endsWith(".exb")) {
            NodeList transcrNames = doc.getElementsByTagName("transcription-name");
            if (transcrNames.getLength() > 0) {
                String transcriptionName = transcrNames.item(0).getTextContent();

                if (!transcriptionName.equals(filename.substring(0, filename.lastIndexOf(".")))) {
                    String message = "Transcription name " + transcriptionName + " does not match the filename";
                    ReportItem.createReportItem(true, this.getClass().getSimpleName(), filename, message);
                }
            }
        }
        
        NodeList reffiles = doc.getElementsByTagName("referenced-file");
        Pattern absolutePathPattern = Pattern.compile("^(file:[/\\\\]+)?[A-Za-z]:");
        for (int i = 0; i < reffiles.getLength(); i++) {
            Element reffile = (Element) reffiles.item(i);            
            String url = reffile.getAttribute("url");
            if (!url.isEmpty()) {
                
                if (filename.endsWith(".exb")) {
                    if (!url.substring(0, url.lastIndexOf(".")).equals(filename.substring(0, filename.lastIndexOf(".")))) {
                        String message = "The name of referenced file " + url + " does not match the filename";
                        ReportItem.createReportItem(true, this.getClass().getSimpleName(), filename, message);
                    }
                }
                
                if (absolutePathPattern.matcher(url).find()) {
                    String message = "Referenced-file " + url
                            + " points to absolute local path, fix to relative path first";
                    ReportItem.createReportItem(true, this.getClass().getSimpleName(), filename, message);
                }
                boolean found = false;
                File justFile = new File(url);
                if (justFile.exists()) {
                    found = true;
                }
              
                String referencePath = doc.getDocumentURI().substring(0, doc.getDocumentURI().lastIndexOf("/"));
                URL absPath = new URL(referencePath + "/" + url);
                File absFile = new File(absPath.toURI());
                if (absFile.exists()) {
                    found = true;
                }
                if (!found) {
                    String message = "File in referenced-file NOT found: " + url;
                    ReportItem.createReportItem(true, this.getClass().getSimpleName(), filename, message);
                } 
            } 
        }
        
        return doc;
    }      
}
