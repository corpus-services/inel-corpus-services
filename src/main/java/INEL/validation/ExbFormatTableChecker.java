package INEL.validation;

import INEL.ReportItem;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Properties;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

public class ExbFormatTableChecker {

    public ExbFormatTableChecker() {
    }
    
    public static ArrayList<String> availableFor() {
        ArrayList<String> fileTypes = new ArrayList<String> ();
        fileTypes.add("exb");
        return fileTypes;
    }
    
    public Document runChecker(Document doc, String filename, Boolean fix, Properties properties) throws SAXException, IOException, ParserConfigurationException {
        
        NodeList tiers = doc.getElementsByTagName("tier");
        ArrayList<String> tierNames = new ArrayList<>();
        
        for (int i = 0; i < tiers.getLength(); i++) {
            Element tier = (Element) tiers.item(i);
            tierNames.add(tier.getAttribute("id"));
        }
        
        NodeList tierFormats = doc.getElementsByTagName("tier-format");      
        
        Integer i = tierFormats.getLength();
        while (i > 0) {
        //for (int i = 0; i < tierFormats.getLength(); i++) {
            Element tierformat = (Element) tierFormats.item(0);
            String tierref = tierformat.getAttribute("tierref");
            
            if (tierNames.contains(tierref)) {
                tierNames.remove(tierref);
            
                if (fix) {
                    Element newFormat = this.makeTierFormat(doc, tierref);
                    tierformat.getParentNode().appendChild(newFormat);
                    tierformat.getParentNode().removeChild(tierformat);
                }                 
            } else {
                if (fix) {
                    tierformat.getParentNode().removeChild(tierformat);
                }
            }
            i--;
        }
        
        if (!tierNames.isEmpty()) {
            NodeList tierFormatTable = doc.getElementsByTagName("tierformat-table");
            if (tierFormatTable.getLength() > 0) {
                for (int tf = 0; tf < tierNames.size(); tf++) {
                    if (fix) {
                        Element newFormat = this.makeTierFormat(doc, tierNames.get(tf));
                        tierFormatTable.item(0).appendChild(newFormat);
                    } else {
                        String message = "No tier format for tier " + tierNames.get(tf);
                        ReportItem.createReportItem(false, this.getClass().getSimpleName(), filename, message);
                    }
                }
            } else {
                if (fix) {
                    Element newFormatTable = doc.createElement("tierformat-table");
                    for (int tf = 0; tf < tierNames.size(); tf++) {
                        Element newFormat = this.makeTierFormat(doc, tierNames.get(tf));
                        newFormatTable.appendChild(newFormat);
                    }
                    NodeList basicTranscription = doc.getElementsByTagName("basic-transcription");
                    basicTranscription.item(0).appendChild(newFormatTable);
                } else {
                    String message = "No tier format table found";
                    ReportItem.createReportItem(false, this.getClass().getSimpleName(), filename, message);
                }
            }
        }
        
        return doc;
    }
    
    private Element makeTierFormat (Document doc, String tierref) {
        Element newFormat = doc.createElement("tier-format");
        newFormat.setAttribute("tierref", tierref);

        Element rowHeight = doc.createElement("property");
        rowHeight.setAttribute("name", "row-height-calculation");
        rowHeight.setTextContent("Generous");
        newFormat.appendChild(rowHeight);

        Element rowHeightFixed = doc.createElement("property");
        rowHeightFixed.setAttribute("name", "fixed-row-height");
        rowHeightFixed.setTextContent("10");
        newFormat.appendChild(rowHeightFixed);

        Element fontFace = doc.createElement("property");
        fontFace.setAttribute("name", "font-face");
        if (tierref.startsWith("st")) {
            fontFace.setTextContent("Bold");
        } else {
            fontFace.setTextContent("Plain");
        }
        newFormat.appendChild(fontFace);

        Element fontColor = doc.createElement("property");
        fontColor.setAttribute("name", "font-color");
        if (tierref.startsWith("ts")) {
            fontColor.setTextContent("#R00G99B33");
        } else if (tierref.startsWith("tx")) {
            fontColor.setTextContent("#R00G00B99");
        } else if (tierref.startsWith("ltr")) {
            fontColor.setTextContent("red");
        } else if (tierref.startsWith("fr")) {
            fontColor.setTextContent("#RccG00B00");
        } else if (tierref.startsWith("fe")) {
            fontColor.setTextContent("blue");
        } else {
            fontColor.setTextContent("black");
        }
        newFormat.appendChild(fontColor);

        Element chunkBorderStyle = doc.createElement("property");
        chunkBorderStyle.setAttribute("name", "chunk-border-style");
        chunkBorderStyle.setTextContent("solid");
        newFormat.appendChild(chunkBorderStyle);

        Element bgColor = doc.createElement("property");
        bgColor.setAttribute("name", "bg-color");
        bgColor.setTextContent("white");
        newFormat.appendChild(bgColor);

        Element textAlignment = doc.createElement("property");
        textAlignment.setAttribute("name", "text-alignment");
        textAlignment.setTextContent("Left");
        newFormat.appendChild(textAlignment);

        Element chunkBorderColor = doc.createElement("property");
        chunkBorderColor.setAttribute("name", "chunk-border-color");
        chunkBorderColor.setTextContent("#R00G00B00");
        newFormat.appendChild(chunkBorderColor);

        Element chunkBorder = doc.createElement("property");
        chunkBorder.setAttribute("name", "chunk-border");
        newFormat.appendChild(chunkBorder);

        Element fontSize = doc.createElement("property");
        fontSize.setAttribute("name", "font-size");
        fontSize.setTextContent("12");
        newFormat.appendChild(fontSize);

        Element fontName = doc.createElement("property");
        fontName.setAttribute("name", "font-name");
        fontName.setTextContent("Charis SIL");
        newFormat.appendChild(fontName);
        
        return newFormat;
    }
}
