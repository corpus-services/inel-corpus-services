package INEL.validation;

import INEL.ReportItem;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Properties;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

public class ExbMcTierChecker {

    public ExbMcTierChecker() {
    }
    
    public static ArrayList<String> availableFor() {
        ArrayList<String> fileTypes = new ArrayList<String> ();
        fileTypes.add("exb");
        return fileTypes;
    }
    
    public Document runChecker(Document doc, String filename, Boolean fix, Properties properties) throws SAXException, IOException, ParserConfigurationException {
        
        NodeList tiers = doc.getElementsByTagName("tier");
        
        for (int i = 0; i < tiers.getLength(); i++) {
            Element tier = (Element) tiers.item(i);
            
            if (tier.getAttribute("category").equals("mc")) {
                
                NodeList mcEvents = tier.getElementsByTagName("event");
                
                for (int e = 0; e < mcEvents.getLength(); e++) {
                    Element event = (Element) mcEvents.item(e);
                    String eventText = event.getTextContent();
                    
                    if (eventText.contains("<NotSure>")) {
                        if (fix) {
                            eventText = eventText.replaceAll("<NotSure>", "%%");
                            event.setTextContent(eventText);
                        } else {
                            String message = "An mc tier event " + eventText + " at " + event.getAttribute("start") + 
                                    " contains NotSure. Needs to be replaced with %%";
                            ReportItem.createReportItem(true, this.getClass().getSimpleName(), filename, message);
                        }
                    }
                }
            }
        }
        
        return doc;
    }      
}
