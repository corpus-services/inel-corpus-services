package INEL.validation;

import INEL.ReportItem;
import java.io.IOException;
import java.net.URISyntaxException;
import org.apache.commons.lang3.tuple.Triple;
import java.util.ArrayList;
import java.util.Properties;
import java.util.regex.Pattern;
import javax.xml.parsers.ParserConfigurationException;
import org.apache.commons.lang3.StringUtils;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

public class ExbMorphemeAndGlossTiersChecker {

    public ExbMorphemeAndGlossTiersChecker() {
    }
    
    public static ArrayList<String> availableFor() {
        ArrayList<String> fileTypes = new ArrayList<String> ();
        fileTypes.add("exb");
        return fileTypes;
    }
    
    public Document runChecker(Document doc, String filename, Boolean fix, Properties properties) throws SAXException, IOException, ParserConfigurationException, URISyntaxException {
        
        NodeList tiers = doc.getElementsByTagName("tier");
        ArrayList<Triple> mbTierInfo = new ArrayList<>();
        Pattern nullMorphemePattern = Pattern.compile("(.*[^\\.]\\[.*)");
        
        for (int i = 0; i < tiers.getLength(); i++) {
            Element tier = (Element) tiers.item(i);
            if (tier.getAttribute("category").equals("mb")) {
                NodeList events = tier.getElementsByTagName("event");
                for (int e = 0; e < events.getLength(); e++) {
                    Element event = (Element) events.item(e);
                    String evText = event.getTextContent();
                    
                    String start = event.getAttribute("start");
                    String end = event.getAttribute("end");
                    Integer breaks = StringUtils.countMatches(evText,"-");
                    breaks = breaks + StringUtils.countMatches(evText,"=");
                    
                    Triple<String, String, Integer> mbTriple = Triple.of(start, end, breaks);
                    mbTierInfo.add(mbTriple);
                    
                    if (evText.contains("…")) {
                        String message = "Found ellipsis … in the event " + evText + 
                                " in the tier " + tier.getAttribute("display-name") + " at " + start;
                        ReportItem.createReportItem(true, this.getClass().getSimpleName(), filename, message);
                    }
                    if (nullMorphemePattern.matcher(evText).find()) {
                        String message = "Found null morpheme in the event " + evText + 
                                " in the tier " + tier.getAttribute("display-name") + " at " + start;
                        ReportItem.createReportItem(true, this.getClass().getSimpleName(), filename, message);
                    }
                }
            }
        }
        
        for (int i = 0; i < tiers.getLength(); i++) {
            Element tier = (Element) tiers.item(i);
            String category = tier.getAttribute("category");
            if (category.matches("mp|mc|ge|gg|gr")) {
                NodeList events = tier.getElementsByTagName("event");
                for (int e = 0; e < events.getLength(); e++) {
                    Element event = (Element) events.item(e);
                    String evText = event.getTextContent();
                    
                    String start = event.getAttribute("start");
                    String end = event.getAttribute("end");
                    Integer breaks = StringUtils.countMatches(evText,"-");
                    breaks = breaks + StringUtils.countMatches(evText,"=");
                    
                    Triple<String, String, Integer> tripleToCheck = Triple.of(start, end, breaks);
                    if (!mbTierInfo.contains(tripleToCheck)) {
                        String message = "the number of hyphens does not match the number of hyphens in matching mb tier " +
                                "for event at " + start + "; event text: " + evText + "; tier: " + category;
                        ReportItem.createReportItem(true, this.getClass().getSimpleName(), filename, message);
                    }
                    
                    if (evText.contains("…")) {
                        String message = "Found ellipsis … in the event " + evText + 
                                " in the tier " + tier.getAttribute("display-name") + " at " + start;
                        ReportItem.createReportItem(true, this.getClass().getSimpleName(), filename, message);
                    }
                    if (nullMorphemePattern.matcher(evText).find()) {
                        String message = "Found null morpheme in the event " + evText + 
                                " in the tier " + tier.getAttribute("display-name") + " at " + start;
                        ReportItem.createReportItem(true, this.getClass().getSimpleName(), filename, message);
                    }
                }
            }
        }
        
        return doc;
    }      
}
