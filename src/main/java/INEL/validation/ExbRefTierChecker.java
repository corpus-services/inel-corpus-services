package INEL.validation;

import INEL.ReportItem;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Properties;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

public class ExbRefTierChecker {

    public ExbRefTierChecker() {
    }
    
    public static ArrayList<String> availableFor() {
        ArrayList<String> fileTypes = new ArrayList<String> ();
        fileTypes.add("exb");
        return fileTypes;
    }
    
    public Document runChecker(Document doc, String filename, Boolean fix, Properties properties) throws SAXException, IOException, ParserConfigurationException {
        
        String transcriptName;
        if (doc.getElementsByTagName("transcription-name").getLength() > 0) {   // check if transcript name exists for the exb file
            transcriptName = doc.getElementsByTagName("transcription-name").item(0).getTextContent(); // get transcript name
        } else {
            transcriptName = "Nameless Transcript";
        }

        NodeList tiers = doc.getElementsByTagName("tier"); // get all tiers of the transcript      
        ArrayList<Element> refTiers = new ArrayList();
        ArrayList<String> speakerNames = new ArrayList();
        for (int i = 0; i < tiers.getLength(); i++) { // loop for dealing with each tier
            Element tier = (Element) tiers.item(i);
            String category = tier.getAttribute("category"); // get category  
            String speakerName = tier.getAttribute("speaker"); // get speaker name
            if (category.equals("ref")) {
                refTiers.add(tier);
                speakerNames.add(speakerName);
            }
        }

        // when there is no reference tier present
        if (refTiers.isEmpty()) {
            String message = "There is no reference tier present in transcript " + transcriptName;
            ReportItem.createReportItem(true, this.getClass().getSimpleName(), filename, message);
        } // when there are reference tier/s present
        else {

            // iterate ref tiers
            for (int i = 0; i < refTiers.size(); i++) {
                NodeList events = refTiers.get(i).getElementsByTagName("event");
                String tierId = refTiers.get(i).getAttribute("id");
                String tierSpeaker = refTiers.get(i).getAttribute("speaker");
                int order = 1;

                // iterate ref events
                for (int j = 0; j < events.getLength(); j++) {
                    Element event = (Element) events.item(j);
                    String eventStart = event.getAttribute("start");
                    String eventEnd = event.getAttribute("end");
                    String wholeRef = event.getTextContent();
                    String eventReference = "event " + eventStart + "/" + eventEnd + ", tier '" + tierId + "', EXB '" + transcriptName + "'";

                    if (wholeRef.contains(".")) {
                        
                        // check if the first part of the ref tier text is identical to the filename
                        
                        String refFname = wholeRef.substring(0, wholeRef.indexOf("."));
                        if (!refFname.equals(filename.substring(0, filename.lastIndexOf(".")))) {
                            String message = "Ref tier value '" + wholeRef + " does not match the filename " + filename;
                            ReportItem.createReportItem(true, this.getClass().getSimpleName(), filename, message);
                            /** old if condition that fixed ref names a bit too ardently 
                            
                            if (fix) {
                                String fixedRef = wholeRef.replace(refFname, filename.substring(0, filename.lastIndexOf(".")));
                                event.setTextContent(fixedRef);
                            } */
                        }

                        // get position of character after number that shall be tested/updated
                        int end = wholeRef.length();
                        if (wholeRef.contains("(")) {
                            end = wholeRef.indexOf("(") - 1;
                        }

                        // get position of first character that belongs to number in question 
                        int start = wholeRef.substring(0, end).lastIndexOf(".") + 1;

                        // get the number in question
                        String no = wholeRef.substring(start, end);
                        try 
                        {                          
                            int numbering = Integer.parseInt(no);
                            // test for correct numbering
                            if (order != numbering) {
                                if (fix) {
                                    String correctNo = String.format("%0" + no.length() + "d", order);
                                    String correctRef = wholeRef.substring(0, start) + correctNo + wholeRef.substring(end);
                                    event.setTextContent(correctRef);
                                } else {
                                    String message = "False numbering in ref ID '" + wholeRef + "' (" + eventReference + ")";
                                    ReportItem.createReportItem(true, this.getClass().getSimpleName(), filename, message);
                                }
                            }
                        } catch(NumberFormatException nfe) {
                            String message = "Ref ID doesn't end with a number: '" + wholeRef + "' (" + eventReference + ")";
                            ReportItem.createReportItem(true, this.getClass().getSimpleName(), filename, message);
                        }
                        order++;

                        // if there is more than one ref tier then also test speaker codes
                        if (refTiers.size() > 1) {
                            int refEnd = start - 1;
                            int refStart = -1;
                            String speakerCode = null;
                            if (wholeRef.substring(0, refEnd).contains(".")) {
                                refStart = wholeRef.substring(0, refEnd).lastIndexOf(".") + 1;
                                speakerCode = wholeRef.substring(refStart, refEnd);
                            }

                            if (speakerCode != null) {
                                if (!speakerCode.equals(tierSpeaker)) {
                                    if (fix) {
                                        String correctRef = event.getTextContent().substring(0, refStart) + tierSpeaker + event.getTextContent().substring(refEnd);
                                        event.setTextContent(correctRef);
                                    } else {
                                        String message = "False speaker code in ref ID '" + wholeRef + "' (should be '" + tierSpeaker + "' in " + eventReference + ")";
                                        ReportItem.createReportItem(true, this.getClass().getSimpleName(), filename, message);
                                    }
                                }
                            } else 
                            if (fix) {
                                String correctRef = event.getTextContent().substring(0, start - 1) + "." + tierSpeaker + event.getTextContent().substring(refEnd);
                                event.setTextContent(correctRef);
                            } else {
                                String message = "Missing speaker code in ref ID '" + wholeRef + "' (should contain '" + tierSpeaker + "' in " + eventReference + ")";
                                ReportItem.createReportItem(true, this.getClass().getSimpleName(), filename, message);
                            }
                        }

                    } // ref ID does not contain any "."
                    else {
                        String message = "Unknown format of ref ID '" + wholeRef + "' in " + transcriptName;
                        ReportItem.createReportItem(true, this.getClass().getSimpleName(), filename, message);
                    }
                }
            }
        }
        
        return doc;
    }      
    
}
