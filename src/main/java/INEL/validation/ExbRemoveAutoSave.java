package INEL.validation;

import INEL.ReportItem;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Properties;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

public class ExbRemoveAutoSave {

    public ExbRemoveAutoSave() {
    }
    
    public static ArrayList<String> availableFor() {
        ArrayList<String> fileTypes = new ArrayList<String> ();
        fileTypes.add("exb");
        fileTypes.add("exs");
        return fileTypes;
    }
    
    public Document runChecker(Document doc, String filename, Boolean fix, Properties properties) throws SAXException, IOException, ParserConfigurationException, URISyntaxException {
        
        NodeList udinfo = doc.getElementsByTagName("ud-information");
        for (int i = 0; i < udinfo.getLength(); i++) {
            Element ud = (Element) udinfo.item(i);
            if (ud.hasAttribute("attribute-name")) {
                if (ud.getAttribute("attribute-name").equals("AutoSave")) {
                    if (fix) {
                        ud.getParentNode().removeChild(ud);
                    } else {
                        String message = "Autosave info needs to be removed";
                        ReportItem.createReportItem(true, this.getClass().getSimpleName(), filename, message);
                    }
                }
            }
        }
        
        return doc;
    }      
}
