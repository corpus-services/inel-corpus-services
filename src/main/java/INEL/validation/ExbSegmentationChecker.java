package INEL.validation;

import INEL.ReportItem;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import javax.xml.parsers.ParserConfigurationException;
import org.apache.commons.io.FilenameUtils;
import org.exmaralda.partitureditor.fsm.FSMException;
import org.exmaralda.partitureditor.jexmaralda.BasicTranscription;
import org.exmaralda.partitureditor.jexmaralda.SegmentedTranscription;
import org.exmaralda.partitureditor.jexmaralda.segment.AbstractSegmentation;
import org.jdom.Element;
import org.jdom.JDOMException;
import org.jdom.input.SAXBuilder;
import org.jdom.output.Format;
import org.jdom.output.XMLOutputter;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;

public class ExbSegmentationChecker {

    public ExbSegmentationChecker() {
    }
    
    public static ArrayList<String> availableFor() {
        ArrayList<String> fileTypes = new ArrayList<String> ();
        fileTypes.add("exb");
        return fileTypes;
    }
    
    public Document runChecker(Document doc, String filename, Boolean fix, Properties properties) throws SAXException, IOException, ParserConfigurationException, 
            FSMException, URISyntaxException, JDOMException {
        
        String path2externalfsm = "";
        
        String baseURI = doc.getBaseURI();
        String basePath = "";
        if (baseURI.contains("/")) { //Linux path
            basePath = doc.getBaseURI().substring(0, doc.getBaseURI().lastIndexOf("/") + 1);
        } else if (baseURI.contains("\\")) { //Win path
            basePath = doc.getBaseURI().substring(0, doc.getBaseURI().lastIndexOf("\\") + 1);
        } 
        
        URL docurl = new URL(baseURI);
        File f = new File(docurl.getFile()); 
        
        BasicTranscription bt; 
        AbstractSegmentation segmentation = new org.exmaralda.partitureditor.jexmaralda.segment.HIATSegmentation();

        if (properties.stringPropertyNames().contains("fsm")) {
            segmentation.pathToExternalFSM = properties.getProperty("fsm");
        } 
        org.exmaralda.partitureditor.jexmaralda.sax.BasicTranscriptionSaxReader reader = new org.exmaralda.partitureditor.jexmaralda.sax.BasicTranscriptionSaxReader();
        bt = reader.readFromFile(f.getAbsolutePath());
                
        List v = segmentation.getSegmentationErrors(bt);
        
        if (v.isEmpty()) {
            if (fix){
                SegmentedTranscription st = segmentation.BasicToSegmented(bt);
                st.setEXBSource(baseURI);
                org.exmaralda.partitureditor.jexmaralda.segment.SegmentCountForMetaInformation.count(st);
                String out = st.toXML();
                
                //convert SegmentedTranscription to a JDOM document
                SAXBuilder builder = new SAXBuilder();
                InputStream stream = new ByteArrayInputStream(out.getBytes("UTF-8"));
                org.jdom.Document exsDocument = builder.build(stream);
                
                //get rid of absolute path in EXB-SOURCE
                //we know where to look. so it's hardcoded; the approach miht be subject to change in the future
                List<Element> meta = exsDocument.getRootElement().getChild("head").getChild("meta-information").getChild("ud-meta-information").getChildren("ud-information");
                for (Element e : meta) {
                    if (e.getAttribute("attribute-name").getValue().endsWith("EXB-SOURCE")) {
                        e.setText(filename);
                    }
                }
                
                //pretty-print
                Format prettyFormat = Format.getPrettyFormat();
                prettyFormat.setTextMode(Format.TextMode.PRESERVE);
                prettyFormat.setOmitDeclaration(false); 

                String outfile = FilenameUtils.getFullPath(baseURI) + FilenameUtils.getBaseName(baseURI) + "_s.exs";
                URL outurl = new URL(outfile);
                final File ouf = new File(outurl.toURI());
                if (!ouf.exists()) {
                    ouf.createNewFile();
                }
                try {
                    FileOutputStream fos = new FileOutputStream(ouf);
                    XMLOutputter xmlOutputter = new XMLOutputter(prettyFormat);
                    xmlOutputter.output(exsDocument, fos);
                    fos.close();
                } catch (Exception ex) {
                    System.out.print("Caught " + ex + " when writing the segmented transcription file");
                }     
            }  
        } else {
            for (Object o : v) {
                FSMException fsme = (FSMException) o;
                String message = fsme.getMessage();
                ReportItem.createReportItem(true, this.getClass().getSimpleName(), filename, message);
            }
        }
        
        return doc;
    }      
}