package INEL.validation;

import INEL.ReportItem;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Properties;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

public class ExbSentenceTiersChecker {

    public ExbSentenceTiersChecker() {
    }
    
    public static ArrayList<String> availableFor() {
        ArrayList<String> fileTypes = new ArrayList<String> ();
        fileTypes.add("exb");
        return fileTypes;
    }
    
    public Document runChecker(Document doc, String filename, Boolean fix, Properties properties) throws SAXException, IOException, ParserConfigurationException {
        
        NodeList speakers = doc.getElementsByTagName("speaker");
        ArrayList<String> speakerIds = new ArrayList();
        
        for (int i = 0; i<speakers.getLength(); i++) {
            Element speakerEl = (Element) speakers.item(i);
            String speakerId = speakerEl.getAttribute("id");
            speakerIds.add(speakerId);
        }
        
        NodeList tiers = doc.getElementsByTagName("tier");
        List<String> sentenceTiersCategories = Arrays.asList("ts", "st", "stl", "lte", "fe", "ltg", "fg", "ltr", "fr", "lth", "fh", "nt", "nto");
        
        for (int j = 0; j < speakerIds.size(); j++) {
            String curSpeaker = speakerIds.get(j);
        
            HashMap<String,String> startEndSentences = new HashMap();
            HashMap<String,String> refValues = new HashMap();

            for (int i = 0; i < tiers.getLength(); i++) {
                Element tier = (Element) tiers.item(i);

                if (tier.getAttribute("category").equals("ref") && tier.getAttribute("speaker").equals(curSpeaker)) {

                    NodeList refEvents = tier.getElementsByTagName("event");
                    for (int e = 0; e < refEvents.getLength(); e++) {
                        Element event = (Element) refEvents.item(e);
                        startEndSentences.put(event.getAttribute("start"), event.getAttribute("end"));
                        refValues.put(event.getAttribute("start"), event.getTextContent());
                    }
                }
            }

            for (int i = 0; i < tiers.getLength(); i++) {
                Element tier = (Element) tiers.item(i);

                if (sentenceTiersCategories.contains(tier.getAttribute("category")) && tier.getAttribute("speaker").equals(curSpeaker)) {

                    NodeList sentEvents = tier.getElementsByTagName("event");
                    for (int e = 0; e < sentEvents.getLength(); e++) {
                        Element event = (Element) sentEvents.item(e);
                        String end = event.getAttribute("end");
                        String start = event.getAttribute("start");

                        if (startEndSentences.containsKey(start)) {
                            if (!startEndSentences.get(start).equals(end)) {
                                String message = "Sentence boundaries differ from those of the respective sentence on the ref tier. "
                                        + "Problematic tier: " + tier.getAttribute("category") + "; ref ID: "
                                        + refValues.get(start);
                                ReportItem.createReportItem(true, this.getClass().getSimpleName(), filename, message);
                            }
                        } else {
                            String message = "Sentence starts at the wrong place respective to the reference tier. "
                                    + "Problematic tier:" + tier.getAttribute("category") + "; sentence: "
                                        + event.getTextContent();
                            ReportItem.createReportItem(true, this.getClass().getSimpleName(), filename, message);
                        }
                    }
                }
                //additional clause for CS tier events
                //annotations with "ext" are supposed to match sentences
                //turned off for the time being
                /*
                if (tier.getAttribute("category").equals("CS") && tier.getAttribute("speaker").equals(curSpeaker)) {
                    NodeList sentEvents = tier.getElementsByTagName("event");
                    for (int e = 0; e < sentEvents.getLength(); e++) {
                        Element event = (Element) sentEvents.item(e);
                        if (event.getTextContent().endsWith("ext")) {
                            String end = event.getAttribute("end");
                            String start = event.getAttribute("start");
                            
                            if (startEndSentences.containsKey(start)) {
                                if (!startEndSentences.get(start).equals(end)) {
                                    String message = "Sentence boundaries differ from those of the respective sentence on the ref tier. "
                                        + "Problematic tier: " + tier.getAttribute("category") + "; ref ID: "
                                        + refValues.get(start) + "; TLI: " + start;
                                    ReportItem.createReportItem(true, this.getClass().getSimpleName(), filename, message);
                                }
                            } else {
                                String message = "Sentence starts at the wrong place respective to the reference tier. "
                                    + "Problematic tier:" + tier.getAttribute("category") + "; sentence: "
                                        + event.getTextContent() + "; TLI: " + start;
                                ReportItem.createReportItem(true, this.getClass().getSimpleName(), filename, message);
                            }
                        }
                    } 
                } */  
            }
        }        
        return doc;
    }      
}
