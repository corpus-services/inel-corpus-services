package INEL.validation;

import INEL.ReportItem;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Properties;
import java.util.regex.Pattern;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

public class ExbSpeakerTableChecker {

    public ExbSpeakerTableChecker() {
    }
    
    public static ArrayList<String> availableFor() {
        ArrayList<String> fileTypes = new ArrayList<String> ();
        fileTypes.add("exb");
        return fileTypes;
    }
    
    public Document runChecker(Document doc, String filename, Boolean fix, Properties properties) throws SAXException, IOException, ParserConfigurationException {
        
        NodeList speakers = doc.getElementsByTagName("speaker");
        ArrayList<String> speakerIds = new ArrayList();
        Pattern abbreviaionPattern = Pattern.compile("^[A-Za-z0-9]+$");
        
        for (int i=0; i<speakers.getLength(); i++) {
            Element speakerEl = (Element) speakers.item(i);
            String speakerId = speakerEl.getAttribute("id");
            NodeList ab = speakerEl.getElementsByTagName("abbreviation");
            Element abbr = (Element) ab.item(0);
            speakerIds.add(speakerId);
            String speakerAb = abbr.getTextContent();
            
            if (!abbreviaionPattern.matcher(speakerAb).matches()) {
                String message = "Speaker abbreviation " + speakerAb + " does not conform to pattern ''[A-Za-z0-9]+'";
                ReportItem.createReportItem(true, this.getClass().getSimpleName(), filename, message);
            }
            
            if (!speakerAb.equals(speakerId)) {
                if (fix) {
                    speakerEl.setAttribute("id", speakerAb);
                } else {
                    String message = "Speaker abbreviation " + speakerAb + " and speaker id " + speakerId + " do not match in the speaker table";
                    ReportItem.createReportItem(true, this.getClass().getSimpleName(), filename, message);
                }
            }
        }
        
        NodeList tierList = doc.getElementsByTagName("tier");
        
        for (int i=0; i<tierList.getLength(); i++) {
            Element tier = (Element) tierList.item(i);
            String speakerTier = tier.getAttribute("speaker");
            String tierId = tier.getAttribute("id");
            if (!speakerIds.contains(speakerTier)) {
                if (fix && speakerIds.size() == 1) {
                    tier.setAttribute("speaker", speakerIds.get(0));
                } else {
                    String message = "No corresponding id in the speaker table was found for speaker attribute of the tier " + tierId;
                    ReportItem.createReportItem(true, this.getClass().getSimpleName(), filename, message);
                }
            }
        }
        
        return doc;
    }      
}
