package INEL.validation;

import INEL.ReportItem;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Properties;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

public class ExbStructureAndTranscriptionTierChecker {

    public ExbStructureAndTranscriptionTierChecker() {
    }
    
    public static ArrayList<String> availableFor() {
        ArrayList<String> fileTypes = new ArrayList<String> ();
        fileTypes.add("exb");
        return fileTypes;
    }
    
    public Document runChecker(Document doc, String filename, Boolean fix, Properties properties) throws SAXException, IOException, ParserConfigurationException, URISyntaxException {
        
        NodeList speakers = doc.getElementsByTagName("speaker");
        ArrayList<String> speakerIds = new ArrayList();
        
        for (int i=0; i<speakers.getLength(); i++) {
            Element speakerEl = (Element) speakers.item(i);
            String speakerId = speakerEl.getAttribute("id");
            speakerIds.add(speakerId);
        }
        
        NodeList tiers = doc.getElementsByTagName("tier");
        HashMap<String, HashMap> mappedTranscriptionTiers = new HashMap<>();
        
        for (int j = 0; j < speakerIds.size(); j++) {
            String curSpeaker = speakerIds.get(j);
            ArrayList<String> startIds = new ArrayList<>();
            ArrayList<String> endIds = new ArrayList<>();
            
            for (int i = 0; i < tiers.getLength(); i++) {
                Element curTier = (Element) tiers.item(i);
                if (curTier.getAttribute("type").equals("t") && curTier.getAttribute("speaker").equals(curSpeaker)) {
                    NodeList events = curTier.getElementsByTagName("event");
                    for (int ev = 0; ev < events.getLength(); ev++) {
                        Element event = (Element) events.item(ev);
                        startIds.add(event.getAttribute("start"));
                        endIds.add(event.getAttribute("end"));
                        
                        //replacing ... with an ellipsis (…) here
                        if (event.getTextContent().contains("...")) {
                            if (fix) {
                                String repl = event.getTextContent().replaceAll("\\.\\.\\.", "…");
                                event.setTextContent(repl);
                            } else {
                                String message = "... needs to be replaced with … in the event: " + event.getTextContent()
                                        + " at " + event.getAttribute("start");
                                ReportItem.createReportItem(true, this.getClass().getSimpleName(), filename, message);
                            }
                        }
                        
                        //all transcription tier events should end with a whitespace
                        if (!event.getTextContent().endsWith(" ")) {
                            if (fix) {
                                event.setTextContent(event.getTextContent() + " ");
                            } else {
                                String message = "No whitespace at the end of the event " + event.getTextContent()
                                        + " at " + event.getAttribute("start");
                                ReportItem.createReportItem(true, this.getClass().getSimpleName(), filename, message);
                            }
                        }
                    }
                }
            }
            
            for (int i = 0; i < tiers.getLength(); i++) {
                Element curTier = (Element) tiers.item(i);
                if (!curTier.getAttribute("type").equals("t") && curTier.getAttribute("speaker").equals(curSpeaker)) {
                    NodeList events = curTier.getElementsByTagName("event");
                    for (int ev = 0; ev < events.getLength(); ev++) {
                        Element event = (Element) events.item(ev);
                        if (!startIds.contains(event.getAttribute("start"))) {
                            String message = "Annotation mismatch: event " + event.getAttribute("start") + 
                                    " at tier " + curTier.getAttribute("id") + " for speaker " + curSpeaker;
                            ReportItem.createReportItem(true, this.getClass().getSimpleName(), filename, message);
                        } else if (!endIds.contains(event.getAttribute("end"))) {
                            String message = "Annotation mismatch: event " + event.getAttribute("end") + 
                                    " at tier " + curTier.getAttribute("id") + " for speaker " + curSpeaker;
                            ReportItem.createReportItem(true, this.getClass().getSimpleName(), filename, message);
                        }                        
                    }
                }
            }
        }
        
        return doc;
    }
}
