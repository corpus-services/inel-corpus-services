package INEL.validation;

import INEL.ReportItem;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Properties;
import org.apache.commons.lang3.tuple.Pair;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

public class ExbTiernameChecker {

    public ExbTiernameChecker() {
    }
    
    public static ArrayList<String> availableFor() {
        ArrayList<String> fileTypes = new ArrayList<String> ();
        fileTypes.add("exb");
        return fileTypes;
    }
    
    public Document runChecker(Document doc, String filename, Boolean fix, Properties properties) throws SAXException, IOException, ParserConfigurationException {
        
        NodeList speakers = doc.getElementsByTagName("speaker");
        NodeList tiers = doc.getElementsByTagName("tier");
        ArrayList<Pair> duplicatesCheck = new ArrayList<>();
        ArrayList<String> tierIds = new ArrayList<>();
        for (int i = 0; i < tiers.getLength(); i++) {
            Element tier = (Element) tiers.item(i);
            String category = tier.getAttribute("category");
            String speaker = tier.getAttribute("speaker"); 
            String displayName = tier.getAttribute("display-name");
            String id = tier.getAttribute("id");
            
            //checking for duplicate tier IDs
            if (tierIds.contains(id)) {
                String message = "Duplicate tier IDs for " + category + " and " + id;
                ReportItem.createReportItem(true, this.getClass().getSimpleName(), filename, message);
            } else {
                tierIds.add(id);
            }
            
            //checking for duplicate tiers
            if (category.equals("t")) {
                Pair<String, String> catSp = Pair.of(category, speaker);
                if (duplicatesCheck.contains(catSp)) {
                    String message = "More than one transcription tier for one "
                        + "speaker. Tier: " + id + "Open in PartiturEditor, "
                        + "change tier type or merge tiers.";
                    ReportItem.createReportItem(true, this.getClass().getSimpleName(), filename, message);
                }
                duplicatesCheck.add(catSp);
            }
            
            //checking for oprhaned tiers
            if (speaker.isBlank() | speaker.isEmpty()) {
                String message = "Orphaned tier:" + id;
                ReportItem.createReportItem(true, this.getClass().getSimpleName(), filename, message);
            }
            
            //checking display-name and ID formatting
            if (speakers.getLength() == 1) {
                if (!displayName.equals(category)) {        
                    if (fix) {
                        tier.setAttribute("display-name", category);
                    } else {
                        String message = "Wrong tier-display-name for tier " + category;
                        ReportItem.createReportItem(true, this.getClass().getSimpleName(), filename, message);
                    }
                }
                if (!id.equals(category)) {
                    if (fix) {
                            tier.setAttribute("id", category);
                    } else {
                        String message = "Wrong ID for tier " + category;
                        ReportItem.createReportItem(true, this.getClass().getSimpleName(), filename, message);
                    }
                }
            } else {
                if (!displayName.equals(category + "-" + speaker)) {
                    if (fix) {
                            tier.setAttribute("display-name", category + "-" + speaker);
                    } else {
                        String message = "Wrong tier-display-name for tier " + category;
                        ReportItem.createReportItem(true, this.getClass().getSimpleName(), filename, message);
                    }
                }
                if (!id.equals(category + "-" + speaker)) {
                    if (fix) {
                            tier.setAttribute("id", category + "-" + speaker);
                    } else {
                        String message = "Wrong ID for tier " + category;
                        ReportItem.createReportItem(true, this.getClass().getSimpleName(), filename, message);
                    }
                }
            } 
        }
        
        return doc;
    }    
}