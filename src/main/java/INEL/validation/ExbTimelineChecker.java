package INEL.validation;

import INEL.ReportItem;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Properties;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;
import javax.xml.xpath.XPathExpressionException;

public class ExbTimelineChecker {

    public ExbTimelineChecker() {
    }
    
    public static ArrayList<String> availableFor() {
        ArrayList<String> fileTypes = new ArrayList<> ();
        fileTypes.add("exb");
        return fileTypes;
    }
    
    public Document runChecker(Document doc, String filename, Boolean fix, Properties properties) throws SAXException, IOException, ParserConfigurationException, URISyntaxException,
            XPathExpressionException {
        
        NodeList timeline = doc.getElementsByTagName("tli");
        Float curTime = Float.valueOf("0.0");
        Boolean TLnormalized = true;
        
        for (int i = 0; i < timeline.getLength(); i++) {
            Element tli = (Element) timeline.item(i);
            
            //check if timeline is normalized
            if (TLnormalized) {
                if (!tli.getAttribute("id").equals("T" + String.valueOf(i))) {
                    TLnormalized = false;
                }
            }
            
            //check for empty timestamps and temporal anomalies
            if (tli.hasAttribute("time")) {
                if (tli.getAttribute("time").isBlank()) {
                    if (fix) {
                        tli.removeAttribute("time");
                    } else {
                        String message = "Empty timestamp at " + tli.getAttribute("id");
                        ReportItem.createReportItem(true, this.getClass().getSimpleName(), filename, message);
                    }
                } else {
                    if (Float.valueOf(tli.getAttribute("time")) < curTime) {
                        String message = "Temporal anomaly at " + tli.getAttribute("id");
                        ReportItem.createReportItem(true, this.getClass().getSimpleName(), filename, message);
                    } else {
                        curTime = Float.valueOf(tli.getAttribute("time"));
                    }
                }
            }    
        }
        
        if (!TLnormalized) {
            String message = "Timeline is not normalized";
            ReportItem.createReportItem(false, this.getClass().getSimpleName(), filename, message);
        }

        return doc;
    }      
}
