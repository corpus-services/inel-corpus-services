package INEL.validation;

import INEL.ReportItem;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;
import java.util.regex.Pattern;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

public class ExbTranslationTiersChecker {

    public ExbTranslationTiersChecker() {
    }
    
    public static ArrayList<String> availableFor() {
        ArrayList<String> fileTypes = new ArrayList<String> ();
        fileTypes.add("exb");
        return fileTypes;
    }
    
    public Document runChecker(Document doc, String filename, Boolean fix, Properties properties) throws SAXException, IOException, ParserConfigurationException {
        
        NodeList tiers = doc.getElementsByTagName("tier");
        List<String> translTierNames = Arrays.asList("fe", "fg", "fh", "fr");
        Pattern brackets = Pattern.compile("\\(\\([^\\)]*\\)\\)");
        
        for (int i = 0; i < tiers.getLength(); i++) {
            Element tier = (Element) tiers.item(i);
            
            if (translTierNames.contains(tier.getAttribute("category"))) {
                
                NodeList mcEvents = tier.getElementsByTagName("event");
                
                for (int e = 0; e < mcEvents.getLength(); e++) {
                    Element event = (Element) mcEvents.item(e);
                    String eventText = event.getTextContent();
                    
                    if (brackets.matcher(eventText).find()) {
                        String message = "Found double brackets in translation tier in event " + eventText + " at " +
                                event.getAttribute("start");
                        ReportItem.createReportItem(false, this.getClass().getSimpleName(), filename, message);
                    }
                }
            }
        }
        
        return doc;
    }      
}
