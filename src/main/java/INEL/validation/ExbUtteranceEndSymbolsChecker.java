package INEL.validation;

import INEL.ReportItem;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Properties;
import java.util.regex.Pattern;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

public class ExbUtteranceEndSymbolsChecker {

    public ExbUtteranceEndSymbolsChecker() {
    }
    
    public static ArrayList<String> availableFor() {
        ArrayList<String> fileTypes = new ArrayList<String> ();
        fileTypes.add("exb");
        return fileTypes;
    }
    
    public Document runChecker(Document doc, String filename, Boolean fix, Properties properties) throws SAXException, IOException, ParserConfigurationException {
        
        NodeList tiers = doc.getElementsByTagName("tier");
        //utterance end symbols are hardcoded here
        //BEWARE
        Pattern utteranceEndSymbols = Pattern.compile("[\\.\\!\\?…]");
        Pattern doubleBracketPattern = Pattern.compile(".*\\(\\([^\\)]*[\\\\.\\\\!\\\\?…][^\\)]*\\)\\).*");
        Pattern whitespaceBeforeUtteranceEnd = Pattern.compile(" [\\.\\!\\?…]");
        Pattern whitespaceWholeEvent = Pattern.compile("^[\\.\\!\\?…] $");
        Pattern whitespaceCorrect = Pattern.compile("[\\.\\!\\?…][\"”]{0,1} $");
        
        NodeList speakers = doc.getElementsByTagName("speaker");
        ArrayList<String> speakerIds = new ArrayList();
        
        for (int i = 0; i<speakers.getLength(); i++) {
            Element speakerEl = (Element) speakers.item(i);
            String speakerId = speakerEl.getAttribute("id");
            speakerIds.add(speakerId);
        }
        
        for (int j = 0; j < speakerIds.size(); j++) {
            String curSpeaker = speakerIds.get(j);
            ArrayList<String> refValues = new ArrayList<>();
            ArrayList<String> endTimes = new ArrayList<>();
        
            for (int i = 0; i < tiers.getLength(); i++) {
                Element tier = (Element) tiers.item(i);

                if (tier.getAttribute("category").equals("ref") && tier.getAttribute("speaker").equals(curSpeaker)) {

                    NodeList refEvents = tier.getElementsByTagName("event");
                    for (int e = 0; e < refEvents.getLength(); e++) {
                        Element event = (Element) refEvents.item(e);
                        refValues.add(event.getTextContent());
                        endTimes.add(event.getAttribute("end"));
                    }
                }
            }

            for (int i = 0; i < tiers.getLength(); i++) {
                Element tier = (Element) tiers.item(i);
                Boolean endSentenceSwitch = false;

                if (tier.getAttribute("category").equals("tx") && tier.getAttribute("speaker").equals(curSpeaker)) {

                    NodeList txEvents = tier.getElementsByTagName("event");
                    int sentenceCount = 0;
                    for (int e = 0; e < txEvents.getLength(); e++) {
                        Element event = (Element) txEvents.item(e);
                        String eventText = event.getTextContent();
                        String end = event.getAttribute("end");
                        String start = event.getAttribute("start");

                        if (endSentenceSwitch) {
                            sentenceCount ++;
                            endSentenceSwitch = false;
                        }

                        if (endTimes.contains(end)) {
                            endSentenceSwitch = true;
                        }

                        if (utteranceEndSymbols.matcher(eventText).find()) {
                            if (!doubleBracketPattern.matcher(eventText).find()) { //skip the symbols in double parentheses
                                if (endTimes.contains(end)) {
                                    if (whitespaceBeforeUtteranceEnd.matcher(eventText).find()) {
                                        if (fix) {
                                            String updText = eventText.replaceAll(" (?=[\\.\\!\\?…])", "");
                                            event.setTextContent(updText);
                                        } else {
                                            String message = "Whitespace appearing in front of utterance end symbol in event " 
                                                    + eventText + " at " + start + ". Reference: " + refValues.get(sentenceCount);
                                            ReportItem.createReportItem(true, this.getClass().getSimpleName(), filename, message);
                                        }
                                    } else if (whitespaceWholeEvent.matcher(eventText).find()) {
                                        String message = "Utterance end symbol is the only text content of an event " 
                                                + eventText + " at " + start + ". Reference: " + refValues.get(sentenceCount);
                                        ReportItem.createReportItem(true, this.getClass().getSimpleName(), filename, message);
                                    } else if (!whitespaceCorrect.matcher(eventText).find()) {
                                        String message = "Utterance end symbol does not appear at the end of the event " 
                                                + eventText + " at " + start + ". Reference: " + refValues.get(sentenceCount);
                                        ReportItem.createReportItem(true, this.getClass().getSimpleName(), filename, message);
                                    }
                                } else {
                                    String message = "Utterance end symbol found mid-sentence in the event  " 
                                            + eventText + " at " + start + ". Reference: " + refValues.get(sentenceCount);
                                    ReportItem.createReportItem(true, this.getClass().getSimpleName(), filename, message);
                                }
                            }
                        } else if (endTimes.contains(end)) {
                            String message = "Sentence in the tx tier does not end with an utterance end symbol, check "
                                    + "the event " + eventText + " at " + start + ". Reference: " + refValues.get(sentenceCount);
                            ReportItem.createReportItem(true, this.getClass().getSimpleName(), filename, message);
                        }
                    }
                }
            }
        }        
        return doc;
    }      
}
